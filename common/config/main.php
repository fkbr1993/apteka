<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'language' => 'ru', // Set the language here
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	    'assetManager' => [
		    'appendTimestamp' => true,
	    ],
	    'urlManager' => [
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
		    'rules' => [
			    '<action:(login|signup|requestpasswordreset|resetpassword|logout|index|contact)>' => 'site/<action>',
		    ]
	    ],
//	    'assetManager' => [
//		    'linkAssets' => true,
//	    ],
    ],
	'modules' => [
		'fileProcessor' => [
			'class' => '\metalguardian\fileProcessor\Module',
			'imageSections' => [
				'user' => [
					'avatar' => [
						'width' => 100,
						'height' => 100,
						'action' => 'adaptiveThumbnail',
					],
				],
				'salepoint' => [
					'preview' => [
						'action' => 'thumbnail',
						'width' => 200,
						'height' => 200,
					],
					'logo' => [
						'action' => 'adaptiveThumbnail',
						'width' => 140,
						'height' => 50,
					]
				],
				'banner' => [
					'standard' => [
						'action' => 'adaptiveThumbnail',
						'width' => 400,
						'height' => 300,
					],
					'small' => [
						'action' => 'adaptiveThumbnail',
						'width' => 50,
						'height' => 40,
					],
				],
			]
		],
	],
	'bootstrap' => ['fileProcessor'],
];

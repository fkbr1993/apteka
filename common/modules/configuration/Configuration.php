<?php

namespace common\modules\configuration;

class Configuration extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\configuration\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

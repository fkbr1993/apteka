$(function () {
	// Initialize metsiMenu plugin to sidebar menu
	$('#side-menu').metisMenu();

	$('#product-modal button').on('click', function () {
		$('#product-modal').hide();
	});

	$('#activate-modal button').on('click', function () {
		$('#activate-modal').hide();
	});

	$('#email-modal button').on('click', function () {
		$('#email-modal').hide();
	});

	$('#success-comment-modal').on('click', function () {
		$('#success-comment-modal').modal('hide');
	});

	$('.admin-send-message').on('click', function () {
		$('.modal-title').text('Сообщение для ' + $(this).data('user'));
		$('#admin-user-id').val($(this).data('id'));
	});

	$('.user-delete-sale').on('click', function () {
		if (confirm('Вы действительно хотите удалить?')) {
			return true;
		} else {
			return false;
		}
	});

	$('#admin-limit-table select').on('change', function () {
		insertParam('per-page', $(this).val());
	});

	$('.user-products-amount input').on('change', function () {
		$promo = $(this).parents()[1];
		$('#title-promo-code').show();
		$($promo).next().children().show();
	});

	$('#user-add-product').on('click', function () {
		var button = $(this),
			count = button.data('count');

		$.post('/user/products', {counter: count}, function (r) {
			button.data('count', count + 1);
			$(r).insertBefore(button);
		});

	});

	$(document).on('click', '.delete-product', function () {
		$(this).parents('.product-container').remove()
	});

	$('#officeform-phone, #user-phone, #signupform-phone').inputmask(
		{
			"mask": "7 (999) 999-99-99"
		}
	);

	// Add special class to minimalize page elements when screen is less than 768px
	setBodySmall();

	// Handle minimalize sidebar menu
	$('.hide-menu').click(function (event) {
		event.preventDefault();
		if ($(window).width() < 769) {
			$("body").toggleClass("show-sidebar");
		} else {
			$("body").toggleClass("hide-sidebar");
		}
	});
	$(window).bind("resize click", function () {

		// Add special class to minimalize page elements when screen is less than 768px
		setBodySmall();

		// Waint until metsiMenu, collapse and other effect finish and set wrapper height
		setTimeout(function () {
			fixWrapperHeight();
		}, 300);
	})

	function setBodySmall() {
		if ($(this).width() < 769) {
			$('body').addClass('page-small');
		} else {
			$('body').removeClass('page-small');
			$('body').removeClass('show-sidebar');
		}
	}

	function fixWrapperHeight() {

		// Get and set current height
		var headerH = 62;
		var navigationH = $("#navigation").height();
		var contentH = $(".content").height();

		// Set new height when contnet height is less then navigation
		if (contentH < navigationH) {
			$("#wrapper").css("min-height", navigationH + 'px');
		}

		// Set new height when contnet height is less then navigation and navigation is less then window
		if (contentH < navigationH && navigationH < $(window).height()) {
			$("#wrapper").css("min-height", $(window).height() - headerH + 'px');
		}

		// Set new height when contnet is higher then navigation but less then window
		if (contentH > navigationH && contentH < $(window).height()) {
			$("#wrapper").css("min-height", $(window).height() - headerH + 'px');
		}
	}

	function insertParam(key, value) {
		key = encodeURI(key);
		value = encodeURI(value);

		var kvp = document.location.search.substr(1).split('&');

		var i = kvp.length;
		var x;
		while (i--) {
			x = kvp[i].split('=');

			if (x[0] == key) {
				x[1] = value;
				kvp[i] = x.join('=');
				break;
			}
		}

		if (i < 0) {
			kvp[kvp.length] = [key, value].join('=');
		}

		//this will reload the page, it's likely better to store this until finished
		document.location.search = kvp.join('&');
	}

	$('.user-politic-signup').click(function() {
		var width = window.innerWidth * 0.66 ;
		// define the height in
		var height = width * window.innerHeight / window.innerWidth ;
		// Ratio the hight to the width as the user screen ratio
		window.open(this.href , 'newwindow', 'width=' + width + ', height=' + height + ', top=' + ((window.innerHeight - height) / 2) + ', left=' + ((window.innerWidth - width) / 2) + ', scrollbars=1');

	});
});

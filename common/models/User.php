<?php
namespace common\models;

use promo\models\Promo;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
	/**
	 *
	 */
	const STATUS_DELETED = 0;
	/**
	 *
	 */
	const STATUS_ACTIVE = 10;
	/**
	 *
	 */
	const STATUS_USER = 1;
	/**
	 *
	 */
	const STATUS_NOT_ACTIVE = 2;
	/**
	 *
	 */
	const STATUS_EMAIL_CONFIRMED = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['status', 'default', 'value' => self::STATUS_USER],
			[
				'status',
				'in',
				'range' => [self::STATUS_DELETED, self::STATUS_ACTIVE, self::STATUS_USER, self::STATUS_NOT_ACTIVE, self::STATUS_EMAIL_CONFIRMED]
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::find()->where('id = :id and (status = :user or status = :admin)', ['id' => $id, 'user' => self::STATUS_USER, 'admin' => self::STATUS_ACTIVE])->one();
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return static::find()->where(
			'username = :username and (status = :user or status = :admin)',
			['username' => $username, 'user' => self::STATUS_USER, 'admin' => self::STATUS_ACTIVE]
		)->one();
	}


	/**
	 * Finds user by email
	 *
	 * @param string $email
	 *
	 * @return static|null
	 */
	public static function findByEmail($email)
	{
		return static::find()->where(
			'email = :email and (status = :user or status = :admin)',
			['email' => $email, 'user' => self::STATUS_USER, 'admin' => self::STATUS_ACTIVE]
		)->one();
	}



	/**
	 * @param $username
	 *
	 * @return null|static
	 */
	public static function findAdminByUsername($username)
	{
		return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @param $email
	 *
	 * @return null|static
	 */
	public static function findAdminByEmail($email)
	{
		return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 *
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::find()->where(
			'password_reset_token = :password_reset_token and status != :status',
			[
				'password_reset_token' => $token,
				'status' => self::STATUS_DELETED,
			]
		)->one();
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int)end($parts);

		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	/**
	 * Проверяем пользователь ли это
	 * @return bool
	 */
	public static function isAdmin()
	{
		if (!Yii::$app->user->isGuest) {
			return Yii::$app->user->identity->status === self::STATUS_ACTIVE ? true : false;
		}

		return false;
	}

	/**
	 * Проверяем пользователь ли это
	 * @return bool
	 */
	public static function isUser()
	{
		if (!Yii::$app->user->isGuest) {
			return Yii::$app->user->identity->status === self::STATUS_USER ? true : false;
		}

		return false;
	}

	/**
	 * Получаем аватар для гостя или пользователя
	 *
	 * @return null|string
	 */
	public static function avatar()
	{
		$avatar = '/css/img/user.png';
		if (!Yii::$app->user->isGuest) {
			$avatar = Yii::$app->user->identity->image_id ? \metalguardian\fileProcessor\helpers\FPM::src(
				Yii::$app->user->identity->image_id,
				'user',
				'avatar'
			) : $avatar;
		}

		return $avatar;
	}

	/**
	 * Отправляем письмо с подтверждением email
	 * @return bool
	 */
	public function sendConformation()
	{
		$message = [
			'from' => Yii::$app->params['supportEmail'],
			'to' => $this->email,
			'subject' => 'Подтверждение почты',
			'body' => "Кликните для подтверждения на " . \yii\helpers\Html::a(
					'ссылку',
					Yii::$app->urlManager->createAbsoluteUrl(
						['site/confirm', 'id' => $this->id, 'key' => $this->auth_key]
					)
				)
		];

		return self::sendMail($message);
	}

	/**
	 * Сообщение
	 * @param $message
	 *
	 * Метод отправки письма
	 *
	 * @return bool
	 */
	public static function sendMail($message)
	{
		$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "From: " . $message['from'] . "\r\n";
		if (!mail($message['to'], $message['subject'], $message['body'], $headers)) {
			return false;
		}

		return true;
	}

	/**
	 * @return array|null|ActiveRecord
	 */
	public static function getPrimaryPromo($id){
		return Promo::find()->where(['user_id' => $id])->orderBy('id ASC')->one();
	}
}

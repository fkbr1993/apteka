<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PixedenAsset extends AssetBundle
{
	public $sourcePath = '@common/web';
	public $css = [
		'css/Pe-icon-7-stroke.css',
	];
}

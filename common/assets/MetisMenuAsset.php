<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

class MetisMenuAsset extends AssetBundle
{
	public $sourcePath = '@vendor/onokumus/metismenu/dist';
    public $css = [
        'metisMenu.min.css',
    ];
    public $js = [
		'metisMenu.min.js',
    ];
    public $depends = [
	    'yii\bootstrap\BootstrapPluginAsset',
    ];
}

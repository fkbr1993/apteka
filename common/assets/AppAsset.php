<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $sourcePath = '@common/web/';
    public $css = [
        'css/site.css',
    ];
    public $js = [
		'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	    'yii\bootstrap\BootstrapPluginAsset',
	    'common\assets\MetisMenuAsset',
	    'common\assets\FontAwesomeAsset',
	    'common\assets\PixedenAsset',
	    'yii\widgets\MaskedInputAsset',
    ];
	public $publishOptions = [
		'forceCopy' => true
	];
}

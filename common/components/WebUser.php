<?php
namespace common\components;

use Yii;

class WebUser extends \yii\web\User
{
	protected function afterLogin($identity, $cookieBased, $duration) {
		parent::afterLogin($identity, $cookieBased, $duration);
		$identity->last_auth = time();
		$identity->save();
	}
}

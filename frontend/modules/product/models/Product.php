<?php

namespace product\models;

use discount\models\Discount;
use kartik\builder\Form;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\fileProcessor\behaviors\UploadBehavior;



/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $price
 */
class Product extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['label'], 'required'],
			[['description'], 'string'],
			['ball', 'integer'],
			[['label'], 'string', 'max' => 255],
			['price', 'match', 'pattern' => '/^(?:0|[1-9]\d*)(?:\.\d{2})?$/']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'label' => 'Label',
			'description' => 'Description',
			'price' => 'Price',
			'ball' => 'Balls'
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => true,
					'required' => false,
				]
			]
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiscount()
	{
		return $this->hasOne(Discount::className(), ['product_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->image_id);

		return true;
	}

	/**
	 * @return array
	 */
	public function prepareForm($viewAction = false)
	{
		return $viewAction ? [
			'id',
			'label',
			'price',
			'ball',
			[
				'attribute' => 'Изображение',
				'format' => 'raw',
				'value' => $this->image_id
					? Html::img(
						\metalguardian\fileProcessor\helpers\FPM::originalSrc($this->image_id)
					)
					: null,
			],
		] : [
			'id' => [
				'type' => Form::INPUT_TEXT,
			],
			'label' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'price' => [
				'type' => Form::INPUT_TEXT,
			],
			'ball' => [
				'type' => Form::INPUT_TEXT,
			],
			'imagePreview' => [
				'type' => \kartik\builder\Form::INPUT_RAW,
				'value' => $this->isNewRecord == 'create'
					? null
					: Html::img(
						\metalguardian\fileProcessor\helpers\FPM::originalSrc($this->image_id)
					),
			],
			'image_id' => [
				'label' => 'Изображение',
				'type' => \kartik\builder\Form::INPUT_FILE,
			],
		];
	}
}

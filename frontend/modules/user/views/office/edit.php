<?php
/* @var $this yii\web\View */

$this->title = 'Редактирование моих данных';

$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

?>

<div class="col-lg-5 hpanel">
	<div class="panel-heading">
		<?= Html::encode($user->username) ?>
	</div>
	<div class="panel-body">
		<?php
		$model->phone = $user->phone;
		$model->email = $user->email;
		$model->username = $user->username;
		$model->salepoint_id = $user->salepoint_id;

		$form = \kartik\form\ActiveForm::begin(
			[
				'id' => 'office-edit-form',
				'enableClientValidation' => false,
				'options' => [
					'class' => 'form-horizontal',
					'enctype' => 'multipart/form-data',
				],
				'fieldConfig' => [
					'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
					'labelOptions' => ['class' => 'col-sm-2 control-label'],
				],
			]
		);
		echo
		$form->field($model, 'last_name'),
		$form->field($model, 'username'),
		$form->field($model, 'middle_name'),
		$form->field($model, 'phone')->label('Телефон'),
		$form->field($model, 'email')->label('Email'),
		$form->field($model, 'salepoint_id')->widget(Select2::classname(), [
			'data' => \yii\helpers\ArrayHelper::map($salepoints, 'id', 'address'),
			'options' => ['placeholder' => 'Выберите место работы, адрес организации'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]),
		//$form->field($model, 'image_id')->label('Аватар')->fileInput();
		$form->field($model, 'image_id')->widget(\kartik\file\FileInput::classname(), [
			'pluginOptions' => [
				'showCaption' => false,
				'showRemove' => false,
				'showUpload' => false,
				'browseClass' => 'btn btn-default btn-md-1',
				'browseIcon' => '',
				'browseLabel' =>  'Загрузить фото'
			],
			'options' => ['accept' => 'image/*'],
		])->label('Аватар');
		?>

		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
			</div>
		</div>

		<?php \kartik\form\ActiveForm::end(); ?>
	</div>
</div>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
?>
<div class="product-container">
	<div class="form-group">
		<i class="fa fa-close delete-product"></i>
		<label class="control-label">Продукт</label>
		<?= Select2::widget(
			[
				'model' => $model,
				'attribute' => 'product_id[' . $counter . ']',
				'data' => ArrayHelper::map($products, 'id', 'label'),
				'options' => ['placeholder' => 'Выберите продукт...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]
		); ?>
	</div>
	<div class="form-group">
		<label class="control-label">Количество</label>
		<?=	TouchSpin::widget(
			[
				'model' => $model,
				'attribute' => 'amount[' . $counter . ']',
				'options' => ['placeholder' => 'Количество...'],
				'pluginOptions' => [
					'buttonup_class' => 'btn btn-default bootstrap-touchspin-up',
					'buttondown_class' => 'btn btn-default bootstrap-touchspin-down',
					'buttonup_txt' => '+',
					'buttondown_txt' => '-'
				]
			]
		); ?>
	</div>
</div>

<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use metalguardian\fileProcessor\helpers\FPM;

$this->title = 'Личный кабинет';

$this->params['breadcrumbs'][] = $this->title;
$address = $user->salepoint_id ? $user->salepoint->address : '';
?>
<div class="user-office col-lg-4 animated-panel zoomIn" style="animation-delay: 0.1s;">
	<div class="hpanel hgreen contact-panel">
		<div class="pull-right">
			<?= isset($user->salepoint) && $user->salepoint->image_id ? Html::img(FPM::originalSrc($user->salepoint->image_id), ['class' => 'logo-img']) : '' ?>
		</div>
		<div class="panel-body">
			<img src="<?= \common\models\User::avatar() ?>" class="img-circle m-b" alt="logo">
			<h3><?= Html::encode($user->username) ?></h3>
			<div class="text-muted font-bold m-b-xs"><?= $address ?></div>
		</div>
		<div class="panel-footer contact-footer">
			<div class="row">
				<div class="col-md-4 border-right animated-panel zoomIn">
					<div class="contact-stat">
						<span>Продажи: </span>
						<strong><?= count($user->sales) ?></strong>
					</div>
				</div>
				<div class="col-md-4 border-right animated-panel zoomIn">
					<div class="contact-stat">
						<span>Баллы: </span>
						<strong><?= $user->ball ?></strong>
					</div>
				</div>
				<div class="col-md-4 border-right animated-panel zoomIn">
					<div class="contact-stat">
						<span>Ваш промокод: </span>
						<strong><?= ($promo = \common\models\User::getPrimaryPromo(Yii::$app->getUser()->id)) ? $promo->label : 'Нет промо'?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--<div class="col-lg-5 hpanel">
	<div class="panel-heading">
		<div class="panel-tools">
			<a class="showhide">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="closebox">
				<i class="fa fa-times"></i>
			</a>
		</div>
		<?= Html::encode($user->username) ?>
	</div>
	<div class="panel-body">
		<?php
		//		echo $user->image_id ? Html::img(
		//			\metalguardian\fileProcessor\helpers\FPM::src($user->image_id, 'user', 'small'),
		//			['class' => 'avatar img-circle']
		//		) : '';
		//		echo Html::tag('h5', 'Email: ' . $user->email),
		//		Html::tag('h5', 'Телефон: ' . $user->phone),
		//		Html::tag('h5', 'ФИО: ' . $user->last_name . ' ' . $user->first_name . ' ' . $user->middle_name),
		//		Html::tag('h5', 'Адрес: ' . $address),
		//		Html::tag('h5', 'Всего баллов: ' . $user->ball)
		?>
		<div class="office-button-group">
			<?php Html::a(
				'Редактировать',
				['/user/office/edit'],
				['class' => 'btn btn-primary', 'name' => 'edit-button']
			);
			Html::a(
				'Отправить сообщение',
				'#user-comment-modal',
				['class' => 'btn btn-primary', 'data-toggle' => 'modal']
			) ?>
		</div>
		<?php
		//		if (!empty($comments)) {
		//			echo Html::beginTag('div', ['class' => 'col-lg-offset-1 col-lg-11']),
		//			Html::tag('h3', 'Сообщения');
		//			foreach ($comments as $comment) {
		//				$type = 'alert alert-success';
		//				$who = 'Вы: ';
		//				if ($comment->type) {
		//					$type = 'alert alert-danger';
		//					$who = 'Админ: ';
		//				}
		//				echo Html::tag('p', $who . $comment->content, ['class' => $type]);
		//			}
		//			echo Html::endTag('div');
		//		}
		?>
	</div>
</div> --!>

<?php
$form = ActiveForm::begin(
	[
		'id' => 'comment-form',
		'options' => ['class' => 'form-horizontal'],
		'action' => Url::to(['/user/office/comment']),
	]
); ?>
<!-- Modal -->
<div class="modal fade" id="user-comment-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Сообщение для администрации</h4>
			</div>
			<div class="modal-body">
				<div class="comment-modal-padding">
					<?= $form->field($model, 'label')->label('Заголовок'); ?>
					<?= $form->field($model, 'content')->textarea(['rows' => 5])->label(false); ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php ActiveForm::end(); ?>


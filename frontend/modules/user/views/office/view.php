<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\i18n\Formatter;

$this->title = 'Личный кабинет';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-lg-5 hpanel">
	<div class="panel-heading">
		<?= Html::encode($user->username) ?>
	</div>
	<div class="panel-body">
		<?php
		echo Html::tag('h5', 'Email: ' . $user->email),
		Html::tag('h5', 'Телефон: ' . $user->phone),
		Html::tag('h5', 'Всего баллов: ' . $user->ball),
		Html::tag('h5', 'Последний раз заходил: ' . date('M d, Y', $user->last_auth));
		?>
	</div>
</div>

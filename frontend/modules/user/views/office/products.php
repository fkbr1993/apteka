<?php
/* @var $this yii\web\View */

$this->title = 'Продукция';

$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\touchspin\TouchSpin;

?>
<div class="col-lg-5 hpanel">
	<div class="panel-heading">
		<?= Html::encode($this->title) ?>
	</div>
	<div class="panel-body">
		<?php
		$model->address = Yii::$app->user->identity->salepoint_id;
		$form = ActiveForm::begin(
			[
				'id' => 'products-add-form',
			]
		);
		echo $form->field($model, 'address')->dropDownList(ArrayHelper::map($address, 'id', 'address')),
		$form->field($model, 'product_id[1]')->widget(
			Select2::classname(),
			[
				'data' => ArrayHelper::map($products, 'id', 'label'),
				'options' => ['placeholder' => 'Выберите продукт...'],
				'pluginOptions' => [
					'allowClear' => true
				],
			]
		),
		$form->field($model, 'amount[1]')->widget(
			TouchSpin::classname(),
			[
				'options' => ['placeholder' => 'Количество...'],
				'pluginOptions' => [
					'buttonup_class' => 'btn btn-default bootstrap-touchspin-up',
					'buttondown_class' => 'btn btn-default bootstrap-touchspin-down',
					'buttonup_txt' => '+',
					'buttondown_txt' => '-'
				]
			]
		);
		echo Html::a('Добавить продукт +', '#', ['id' => 'user-add-product', 'data-count' => 2]),
		Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'edit-button']);
		?>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<?php if ($message) : ?>
	<!-- Modal -->
	<div class="modal fade in" id="product-modal" style="display: block">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" data-target="#product-modal" aria-hidden="false">&times;</button>
					<h4 class="modal-title">Продажа совершена</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" data-target="#product-modal">
						Закрыть
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

<?php endif; ?>

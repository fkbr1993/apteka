<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Отправить сообщение администратору';

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-8 hpanel">
	<div class="panel-heading">
		<?= Html::encode($this->title) ?>
	</div>
	<div class="panel-body">
		<div class="m-b-md">
			<?php
			if (!empty($comments)) {
				echo Html::beginTag('div');
				foreach ($comments as $comment) {
					$type = 'alert comment-row-user';
					$who = 'Вы:  ';
					if ($comment->type) {
						$type = 'alert comment-row-admin';
						$who = 'Админ:  ';
					}
					echo Html::beginTag('div', ['class' => $type]),
					Html::tag('p', $comment->label),
					Html::tag('p', $who . $comment->content),
					Html::endTag('div');
				}
				echo Html::endTag('div');
			} ?>
		</div>
		<?php
		$form = ActiveForm::begin(
			[
				'id' => 'comment-form',
				'options' => ['class' => 'form-horizontal'],
				'fieldConfig' => [
					'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
					'labelOptions' => ['class' => 'col-sm-2 control-label'],
				],
				'action' => Url::to(['/user/office/comment']),
			]
		);
		echo $form->field($model, 'label'),
		$form->field($model, 'content')->textarea(['rows' => 5]); ?>

		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
			</div>
		</div>
		<?php ActiveForm::end() ?>

	</div>
</div>

<?php if (Yii::$app->session->getFlash('comment', false)) : ?>

<!-- Modal -->
<div class="modal fade" id="success-comment-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Ваше сообщение успешно отправлено администратору</h4>
			</div>
			<div class="modal-body">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
	<?= $this->registerJs("$('#success-comment-modal').modal('show');") ?>

<!-- /.modal -->
<?php endif ?>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$this->title = 'Редактирование продажи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-5 hpanel">
	<div class="panel-heading">
		<?= Html::encode($this->title) ?>
	</div>
	<div class="panel-body">
		<?php
		$model->product_id = $sale->product_id;
		$model->amount = $sale->amount;
		$model->address = $sale->salepoint_id;
		$form = ActiveForm::begin(
			[
				'id' => 'office-edit-form',
				'options' => ['class' => 'form-horizontal'],
				'fieldConfig' => [
					'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
					'labelOptions' => ['class' => 'col-sm-2 control-label'],
				],
			]
		);
		echo $form->field($model, 'product_id')->dropDownList(ArrayHelper::map($products, 'id', 'label'))->label(
			'Продукт'
		),
		$form->field($model, 'amount')->input('number')->label('Количество'),
		$form->field($model, 'address')->dropDownList(ArrayHelper::map($address, 'id', 'address'))->label('Адрес');
		?>

		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
			</div>
		</div>

		<?php ActiveForm::end(); ?>
	</div>
</div>

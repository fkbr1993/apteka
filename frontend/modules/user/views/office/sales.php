<?php
/* @var $this yii\web\View */

$this->title = 'Продажи';

$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use metalguardian\fileProcessor\helpers\FPM;
use kartik\date\DatePicker;

$balls = 0;
?>
<div class="col-lg-8 hpanel page-user-sale">
	<div class="panel-body">
		<p>Поиск по дате продажи <small class="pull-right">БетулаФарм<sup>®</sup></small></p>
		<?php $form = \yii\widgets\ActiveForm::begin(
			[
				'method' => 'get',
				'action' => ['/user/office/sales'],
			]
		) ?>
		<div class="form-group">
			<div class="input-group">
				<?= DatePicker::widget(
					[
						'model' => $model,
						'attribute' => 'timestamp_from',
						'attribute2' => 'timestamp_to',
						'separator' => 'До',
						'type' => DatePicker::TYPE_RANGE,
						'form' => $form,
						'pluginOptions' => [
							'format' => 'dd/mm/yyyy',
							'autoclose' => true,
						]
					]
				) ?>
				<span class="input-group-btn">
					<button class="btn btn btn-success" type="submit">
						<i class="fa fa-search"></i>
						Поиск
					</button>
				</span>
			</div>
		</div>
		<?php $form->end() ?>
		<div class="table-responsive m-t-lg">
			<table class="table">
				<thead>
				<tr>
					<th>Продукт</th>
					<th></th>
					<th>Количество</th>
					<th>Покупатель</th>
					<th>Баллы</th>
					<th>Цена</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($sales as $sale) :
					$balls += $sale->ball;
					?>
					<tr>
						<td class="col-sm-1 text-center"><?= $sale->product->image_id ? Html::img(FPM::src(
							$sale->product->image_id,
							'banner',
							'small'
							)) : '' ?></td>
						<td><strong><?= $sale->product->label ?></strong><br />
							<small>дата: <?= date('d/m/Y', $sale->created_at) ?></small></td>
						<td><?= $sale->amount ?></td>
						<td><?= $sale->client_name . ' ' . $sale->client_last_name ?></td>
						<td><?= $sale->ball ?></td>
						<td><?= $sale->price . ' <i class="fa fa-rub"></i>' ?></td>
						<td class="text-right">
							<div class="btn-group">
								<?= Html::a(
									'Редактировать',
									['saleedit', 'id' => $sale->id],
									['class' => 'btn btn-default btn-xs']
								) .
								Html::a(
									'Удалить',
									['saledelete', 'id' => $sale->id],
									['class' => 'btn btn-default btn-xs user-delete-sale']
								) ?>
							</div>
						</td>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel-footer">
		Всего баллов: <?= $balls ?>
	</div>
</div>

<?php

namespace user\controllers;

use comment\models\Comment;
use product\models\Product;
use sale\models\Sale;
use salepoint\models\SalePoint;
use user\models\CommentForm;
use user\models\OfficeForm;
use user\models\ProductForm;
use Yii;
use user\models\User;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class OfficeController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
    {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'rules' => [
			        [
				        'actions' => ['view', 'create', 'update', 'delete','index', 'products', 'comment', 'view', 'edit', 'sales', 'saledelete', 'saleedit'],
				        'allow' => true,
				        'roles' => ['@'],
			        ],
		        ],
	        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Выводим данные о пользователе (Личный кабинет)
     * @return mixed
     */
    public function actionIndex()
    {

	    $user = User::find()->where(['id' => Yii::$app->user->id])->with(['sales', 'salepoint'])->one();
	    if ($user) {
		    $model = new CommentForm;
		    return $this->render('index',['user' => $user, 'model' => $model]);
	    }

	    throw new HttpException('404','User not found');
    }


	/**
	 * Выводим форму для формирования продажи.
	 *
	 * AJAX - Вернет поля, если пользователь нажмет добавить поле
	 * POST - Запишет продажи в таблицу
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionProducts() {
		$model = new ProductForm();

		if (Yii::$app->request->isAjax && Yii::$app->request->post('counter')) {
			$products = Product::find()->all();
			return $this->renderAjax('_item',['model' => $model, 'products' => $products, 'counter' => (int)Yii::$app->request->post('counter')]);
		}

		if (Yii::$app->request->isGet) {
			$products = Product::find()->all();
			$address = SalePoint::find()->all();
			$message = Yii::$app->session->getFlash('success',false,true);
			return $this->render('products',['products' => $products, 'model' => $model, 'message' => $message, 'address' => $address]);
		} elseif($model->load(Yii::$app->request->post())) {
			if ($model->update()) {
				Yii::$app->session->setFlash('success', true);

			}
		}

		return $this->redirect('/user/office/products');
	}

	/**
	 * Редактируем данные пользователя
	 *
	 * POST - сохраняет введенное пользователем
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionEdit() {
		$model = new OfficeForm();
		$user = User::findOne(Yii::$app->user->id);
		if (Yii::$app->request->isGet) {
			$salepoints = SalePoint::find()->all();
			return $this->render('edit',['user' => $user, 'model' => $model, 'salepoints' => $salepoints]);
		} else {
			$model->oldUsername = $user->username;
			$model->oldEmail = $user->email;
			if ($model->load(Yii::$app->request->post())) {
				$model->update($user);
			}
		}
		return $this->redirect(['/user/office/index']);
	}

	/**
	 * Смотрим продажи пользователя
	 *
	 * @return string
	 */
	public function actionSales() {
		$sales = Sale::find()->where(['user_id' => Yii::$app->user->id])->joinWith('product','address')->orderBy('sale.id DESC');
		$model = new Sale();
		if ($model->load(Yii::$app->request->get())) {
			if ($model->timestamp_from) {
				$from = strtotime(str_replace('/', '.', $model->timestamp_from));
				$sales->andWhere(['>=', 'sale.created_at', $from]);
			}
			if ($model->timestamp_to) {
				$to = strtotime(str_replace('/', '.', $model->timestamp_to)) + 86399;
				$sales->andFilterWhere(['<=', 'sale.created_at', $to]);
			}
		}

		return $this->render('sales',['sales' => $sales->all(), 'model' => $model]);
	}

	/**
	 * Выводим комментарии предназначенные для пользователя.
	 *
	 * POST - Сохраняем комментарий пользователя адресованный админу
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionComment() {
		$model = new CommentForm;
		if (Yii::$app->request->isGet) {
			$new = Comment::findAll(['user_id' => Yii::$app->user->id, 'type' => 1, 'status' => 0]);
			foreach ($new as $n) {
				$n->status = 1;
				$n->save();
			}
			$comments = Comment::find()->where(['user_id' => Yii::$app->user->id])->joinWith('user')->all();
			return $this->render('comment',['model' => $model, 'comments' => $comments]);
		} elseif ($model->load(Yii::$app->request->post())) {
			$model->update();
			return $this->redirect(['/user/comment']);
		}
	}

	/**
	 * Удаляем продажу
	 */
	public function actionSaledelete() {
		$sale = Sale::findOne(['id' => Yii::$app->request->get('id'), 'user_id' => Yii::$app->user->id]);
		if ($sale) {
			$user = User::findOne($sale->user_id);
			$user->ball = $user->ball - $sale->ball;
			if ($sale->delete()) {
				$user->save();
			}
		}
		$this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * Редактируем продажу
	 *
	 * POST - сохраняет новые параметры для продажи и пересчитывает баллы
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionSaleedit($id) {
		$model = new ProductForm();
		$sale = Sale::findOne(['id' => $id, 'user_id' => Yii::$app->user->id]);
		if (Yii::$app->request->isGet && $sale) {
			$products = Product::find()->all();
			$address = SalePoint::find()->all();
			return $this->render('saleEdit',['sale' => $sale, 'model' => $model, 'products' => $products, 'address' => $address]);
		} elseif($model->load(Yii::$app->request->post()) && $sale) {
			if ($model->edit($sale)) {

			}
		}
		return $this->redirect(['/user/sales']);
	}


}

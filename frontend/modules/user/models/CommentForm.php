<?php
namespace user\models;

use comment\models\Comment;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Product form
 */
class CommentForm extends Model
{

	/**
	 * Тело сообщения
	 * @var
	 */
	public $content;

	/**
	 * Заголовок
	 * @var
	 */
	public $label;

	/**
	 * id пользователя
	 * @var
	 */
	public $user_id;

	/**
	 * Тип
	 * @var
	 */
	public $type;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['content','label'], 'required', 'message' => 'Это поле не может быть пустым'],
			[['content','label'], 'string'],
			['user_id', 'integer', 'skipOnEmpty' => true],
		];
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'username' => 'Пользователь',
			'type' => 'Тип',
			'content' => 'Сообщение',
			'label' => 'Заголовок',
		];
	}

	/**
	 * Сохраняем сообщение
	 * @return bool
	 */
	public function update()
	{
		if ($this->validate()) {
			$comment = new Comment();
			$comment->content = $this->content;
			$comment->user_id = Yii::$app->user->id;
			$comment->type = User::isUser() ? 0 : 1;
			$comment->label = $this->label;
			Yii::$app->session->setFlash('comment', true);
			return $comment->save();
		} else {
			return false;
		}
	}
}

<?php
namespace user\models;

use common\models\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;

/**
 * Office form
 */
class OfficeForm extends Model
{

	/**
	 * Логин
	 * @var
	 */
	public $username;

	/**
	 * Почта
	 * @var
	 */
	public $email;

	/**
	 * Пароль
	 * @var
	 */
	public $password;

	/**
	 * Телефон
	 * @var
	 */
	public $phone;

	/**
	 * Старый логин
	 * @var
	 */
	public $oldUsername;

	/**
	 * Старый email
	 * @var
	 */
	public $oldEmail;

	/**
	 * Аватар
	 * @var
	 */
	public $image_id;

	/**
	 * Имя
	 * @var
	 */
	public $first_name;

	/**
	 * Фамилия
	 * @var
	 */
	public $last_name;

	/**
	 * Отчество
	 * @var
	 */
	public $middle_name;

	/**
	 * Адрес
	 * @var
	 */
	public $salepoint_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['email'], 'required', 'message' => 'Это поле не может быть пустым'],
			['username', 'filter', 'filter' => 'trim'],
			['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Это имя уже используется.', 'filter' => ['!=', 'username', $this->oldUsername]],
			['username', 'string', 'min' => 2, 'max' => 255],

			['email', 'filter', 'filter' => 'trim'],
			['email', 'email'],
			['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот email уже используется.', 'filter' => ['!=', 'email', $this->oldEmail]],

			[['phone', 'first_name', 'last_name', 'middle_name'], 'string'],
			['salepoint_id', 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'Псевдоним',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'middle_name' => 'Отчество',
			'auth_key' => 'Auth Key',
			'password' => 'Пароль',
			'salepoint_id' => 'Адрес',
			'password_hash' => 'Password Hash',
			'password_reset_token' => 'Password Reset Token',
			'email' => 'Email',
			'status' => 'Status',
			'phone' => 'Номер телефона',
			'ball' => 'Баллы',
			'confirm' => 'Вы согласны с условиями?',
			'image_id' => 'Photo ID',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => true,
					'required' => false,
				]
			]
		);
	}


	/**
	 * @param $user \user\models\User
	 *
	 * Обновляем данные пользователя.
	 *
	 * @return bool
	 */
	public function update($user)
    {
	    if ($this->validate() && $user) {
		    if ($this->image_id) {
			    FPM::deleteFile($user->image_id);
		    }
			$user->username = $this->username;
            $user->email = $this->email;
			$user->phone = $this->phone;
			$user->image_id = $this->image_id;
			$user->last_name = $this->last_name;
		    $user->middle_name = $this->middle_name;
			$user->salepoint_id = $this->salepoint_id;
		    if ($user->save()) {
			    return $user;
		    }
	    }
		return false;
    }
}

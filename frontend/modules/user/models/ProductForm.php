<?php
namespace user\models;

use product\models\Product;
use promo\models\Promo;
use sale\models\Sale;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Product form
 */
class ProductForm extends Model
{

	/**
	 * Количество
	 *
	 * @var
	 */
	public $amount;

	/**
	 * Продукт
	 *
	 * @var
	 */
	public $product_id;

	/**
	 * Адрес
	 *
	 * @var
	 */
	public $address;

	/**
	 * @var
	 */
	public $client_name;

	/**
	 * @var
	 */
	public $client_last_name;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['amount', 'product_id'], 'required', 'message' => 'Это поле не может быть пустым'],
			[['amount'], 'isNumberSale', 'skipOnEmpty' => true],
			[['product_id'], 'checkIsArray'],
			['address', 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'amount' => 'Количество',
			'product_id' => 'Продукт',
			'address' => 'Адрес',
		];
	}

	/**
	 * Проверка на число
	 */
	public function isNumberSale()
	{
		if (is_array($this->amount)) {
			foreach ($this->amount as $amount) {
				if (!is_numeric($amount) && !empty($amount)) {
					$this->addError('amount', 'Не число');
				}
			}
		}
	}

	/**
	 * Проверка на массив
	 *
	 * @param $value
	 */
	public function checkIsArray($value)
	{
		if (!is_array($this->$value)) {
			$this->addError($this->$value, 'Не массив');
		}
	}

	/**
	 * @param $products
	 *
	 * Получаем массив продуктов
	 *
	 * @return array
	 */
	private function getIds($products)
	{
		$arr = [];

		foreach ($products as $product) {
			$arr[$product->id] = $product;
		}

		return $arr;
	}

	/**
	 * Преобразовываем полученные данные в один массив.
	 * Ключ - id продукта
	 * Значение - Количество продуктов
	 *
	 * @return array
	 */
	private function convertData()
	{
		$arr = [];

		foreach ($this->product_id as $k => $product) {
			if (isset($this->amount[$k])) {
				$arr[$product] = (int)$this->amount[$k];
			}
		}

		return $arr;
	}


	/**
	 *
	 * Добавляем продажи
	 *
	 * @return bool
	 */
	public function update()
	{
		$filter = $this->convertData();
		$products = Product::find()->where(['id' => array_keys($filter)])->all();
		if ($this->validate()) {
			$products = $this->getIds($products);
			$user = User::findOne(['id' => Yii::$app->user->id]);
			if (!$user) {
				return false;
			}
			$promo = Promo::findOne(['archive' => 0, 'user_id' => $user->id]);
			$discounts = $promo ? $promo->discounts : [];
			$totalUserBalls = 0;
			foreach ($filter as $key => $amount) {
				if (array_key_exists($key, $products)) {
					$sale = new Sale;
					$sale->user_id = Yii::$app->user->id;
					$sale->salepoint_id = $this->address;
					$sale->amount = $amount;
					$sale->client_name = $this->client_name;
					$sale->client_last_name = $this->client_last_name;
					$ball = 0;
					if ($discount = $this->findProductDiscount($discounts, $products[$key]->id)) {
						$ball = $discount->ball ? $discount->ball : 0;
						$totalPrice = ($products[$key]->price - $discount->discount * ($products[$key]->price) / 100) * $amount;
					} else {
						$totalPrice = $products[$key]->price * $amount;
					}
					$totalBalls = ($totalPrice / 100) * $ball;
					$sale->price = $totalPrice;
					$sale->ball = $totalBalls;
					$sale->created_at = time();
					$sale->product_id = $key;
					if ($sale->save()) {
						$totalUserBalls += $totalBalls;
					}
				}
			}
			$user->ball += $totalUserBalls;
			$user->save();

			return true;
		}

		return false;
	}


	/**
	 * @param $sale \sale\models\Sale
	 *
	 * Редактируем продажу
	 *
	 */
	public function edit($sale)
	{
		if ((int)$this->amount > 0) {
			$user = User::findOne(['id' => Yii::$app->user->id]);
			$promo = Promo::findOne(['archive' => 0, 'user_id' => $user->id]);
			$discounts = $promo ? $promo->discounts : [];
			$product = Product::findOne($this->product_id);
			$sale->salepoint_id = $this->address;
			$sale->amount = $this->amount;
			$sale->client_name = $this->client_name;
			$sale->client_last_name = $this->client_last_name;
			$sale->product_id = $this->product_id;
			$user->ball = $user->ball - $sale->ball;
			$ball = 0;
			if ($discount = $this->findProductDiscount($discounts, $product->id)) {
				$ball = $discount->ball ? $discount->ball : 0;
				$totalPrice = ($product->price - $discount->discount * ($product->price) / 100) * $this->amount;
			} else {
				$totalPrice = $product->price * $this->amount;
			}
			$sale->price = $totalPrice;
			$totalBalls = ($totalPrice / 100) * $ball;
			$sale->ball = $totalBalls;
			$user->ball += $totalBalls;
			if ($sale->save()) {
				$user->save();
			}
		}
	}

	/**
	 * @param $discounts
	 * @param $productId
	 *
	 * Ищем скидку для продукта
	 *
	 * @return mixed
	 */
	public function findProductDiscount($discounts, $productId) {
		foreach($discounts as $discount) {
			if ($discount->product_id == $productId) {
				return $discount;
			}
		}

		return false;
	}
}

<?php

namespace user\models;

use sale\models\Sale;
use salepoint\models\SalePoint;
use Yii;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\ArrayHelper;
use kartik\builder\Form;
use yii\helpers\Html;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $salepoint_id
 * @property string $passport
 * @property string $registration
 * @property string $inn
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */

class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'image_id'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'phone',], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            ['ball', 'number']
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'Имя',
			'last_name' => 'Фамилия',
			'middle_name' => 'Отчество',
			'auth_key' => 'Auth Key',
			'password' => 'Пароль',
			'salepoint' => 'Адрес',
			'password_hash' => 'Password Hash',
			'password_reset_token' => 'Password Reset Token',
			'email' => 'Email',
			'status' => 'Статус',
			'phone' => 'Номер телефона',
			'ball' => 'Баллы',
			'confirm' => 'Вы согласны с условиями?',
			'image_id' => 'Аватар',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => true,
					'required' => false,
				]
			]
		);
	}

	/**
	 * Удаляем аватар до удаления пользователя
	 *
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->image_id);

		return true;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalepoint()
	{
		return $this->hasOne(SalePoint::className(), ['id' => 'salepoint_id']);
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSales()
	{
		return $this->hasMany(Sale::className(), ['user_id' => 'id']);
	}
}

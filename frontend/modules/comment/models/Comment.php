<?php

namespace comment\models;

use Yii;
use user\models\User;

/**
 * This is the model class for table "comment".
 *
 * @property integer $user_id
 * @property integer $type
 * @property string $content
 * @property string $label
 */
class Comment extends \yii\db\ActiveRecord
{

	/**
	 * @var
	 */
	public $username;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'integer'],
            [['content', 'username'], 'string'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Пользователь',
            'type' => 'Тип',
            'content' => 'Сообщение',
            'label' => 'Заголовок',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @param $user_id
	 *
	 * Возвращает непрочитанные сообщения
	 *
	 * @return static[]
	 */
	public static function getUnreadMessages($user_id) {
		return self::findAll(['status' => 0, 'user_id' => $user_id, 'type' => 1]);
	}
}

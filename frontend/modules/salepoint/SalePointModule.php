<?php

namespace app\modules\salepoint;

class SalePoint extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\salepoint\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace discount\models;

use Yii;
use product\models\Product;
use promo\models\Promo;

/**
 * This is the model class for table "discount".
 *
 * Модель скидки на продукт. Админ создаёт сначала промокод к провизору, затем к этому промокоду
 * создаются скидки, в которых указывается связанный промокод, связанный продукт и размер скидки
 *
 * @property integer $id
 * @property integer $promo_id идентификатор связанного промокода
 * @property integer $product_id идентификатор связанного продукта
 * @property integer $discount размер скидки в % для данного промокода на данный продукт
 *
 * @property Product $product связанный продукт
 * @property Promo $promo связанный промокод
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['promo_id', 'product_id', 'discount'], 'required'],
	        [['promo_id', 'product_id'], 'integer'],
	        ['discount', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_id' => 'Промо-код',
            'product_id' => 'Продукт',
            'discount' => 'Cкидка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['id' => 'promo_id']);
    }
}

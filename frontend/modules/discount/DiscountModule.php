<?php

namespace discount;

/**
 * Class DiscountModule
 *
 * Cкидки к промокодам,
 * каждая скидка относится к одному товару и одному промокоду
 *
 * @package discount
 */
class DiscountModule extends \yii\base\Module
{
    public $controllerNamespace = 'discount\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

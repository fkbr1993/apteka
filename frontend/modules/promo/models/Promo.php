<?php

namespace promo\models;

use discount\models\Discount;
use user\models\User;
use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $label
 * @property string $date_from
 * @property string $date_to
 * @property string $description
 *
 * @property Discount[] $discounts
 * @property User $user
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['label', 'date_from', 'date_to'], 'required'],
            [['date_from', 'date_to'], 'safe'],
            [['description'], 'string'],
            [['label'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'label' => 'Промо-код',
            'date_from' => 'Начало скидки',
            'date_to' => 'Конец скидки',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['promo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

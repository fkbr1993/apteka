<?php

namespace promo;

/**
 * Class PromoModule
 *
 * @package app\modules\promo
 */
class PromoModule extends \yii\base\Module
{
    public $controllerNamespace = 'promo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

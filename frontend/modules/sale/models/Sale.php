<?php

namespace sale\models;

use product\models\Product;
use salepoint\models\SalePoint;
use user\models\User;
use Yii;

/**
 * This is the model class for table "sale".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property string $amount
 * @property string $ball
 * @property string $salepoint_id
 * @property string $client_name
 * @property string $client_last_name
 * @property string $price
 */
class Sale extends \yii\db\ActiveRecord
{

	/**
	 * Адрес
	 *
	 * @var
	 */
	public $address;

	public $timestamp_from;

	public $timestamp_to;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sale';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'product_id', 'salepoint_id', 'amount'], 'integer'],
			[['created_at', 'timestamp_to', 'timestamp_from', 'client_name', 'client_last_name'], 'safe'],
			['ball', 'number']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'User',
			'label' => 'Product',
			'amount' => 'Amount',
			'address' => 'Address',
			'ball' => 'Balls',
			'client_name' => 'Client name',
			'client_last_name' => 'Client last name',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalepoint()
	{
		return $this->hasOne(SalePoint::className(), ['id' => 'salepoint_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}

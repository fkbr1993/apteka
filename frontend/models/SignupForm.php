<?php
namespace frontend\models;

use common\models\User;
use salepoint\models\SalePoint;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{

	public $username;

	public $email;

	public $password;

	public $phone;

	public $confirm;

	public $first_name;

	public $last_name;

	public $middle_name;

	public $salepoint;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['password', 'email', 'username'], 'required', 'message' => 'Это поле не может быть пустым'],
			['username', 'filter', 'filter' => 'trim'],
			[
				'username',
				'unique',
				'targetClass' => '\common\models\User',
				'message' => 'Этот логин пользователя уже используется'
			],
			['username', 'string', 'min' => 2, 'max' => 255],
			['email', 'filter', 'filter' => 'trim'],
			['email', 'email'],
			['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот email уже используется'],
			['password', 'string', 'min' => 6],
			[['phone', 'first_name', 'last_name', 'middle_name'], 'string'],
			['salepoint', 'safe'],
			['confirm', 'required', 'requiredValue' => 1, 'message' => 'Вы должны согласиться с условиями']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'Имя',
			'last_name' => 'Фамилия',
			'middle_name' => 'Отчество',
			'auth_key' => 'Auth Key',
			'password' => 'Пароль',
			'salepoint' => 'Адрес',
			'password_hash' => 'Password Hash',
			'password_reset_token' => 'Password Reset Token',
			'email' => 'Email',
			'status' => 'Status',
			'phone' => 'Номер телефона',
			'ball' => 'Баллы',
			'confirm' => 'Вы согласны с условиями?',
			'image_id' => 'Photo ID',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		];
	}

	/**
	 * Создаем запись пользователя в базе
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup()
	{
		if ($this->validate()) {

			if (!(int)$this->salepoint) {
				$salePoint = SalePoint::findOne(['address' => $this->salepoint]);
				if ($salePoint) {
					$this->salepoint = $salePoint->id;
				} else {
					$salePoint = new SalePoint();
					$salePoint->address = $this->salepoint;
					if ($salePoint->save()) {
						$this->salepoint = $salePoint->id;
					}
				}
			}

			$user = new User();
			$user->username = $this->username;
			$user->email = $this->email;
//	        $user->first_name = $this->first_name;
//	        $user->last_name = $this->last_name;
//	        $user->middle_name = $this->middle_name;
			$user->phone = $this->phone;
			$user->salepoint_id = $this->salepoint;
			$user->setPassword($this->password);
			$user->generateAuthKey();
			$user->status = User::STATUS_NOT_ACTIVE;
			if ($user->save()) {
				return $user;
			}
		}

		return null;
	}
}

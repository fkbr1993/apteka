<?php
namespace frontend\controllers;

use salepoint\models\SalePoint;
use common\models\User;
use Yii;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup', 'request-password-reset', 'reset-password'],
				'rules' => [
					[
						'actions' => ['signup', 'login', 'contact', 'request-password-reset', 'reset-password', 'confirm', 'error', 'politic'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout', 'index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * @return \yii\web\Response
	 */
	public function actionIndex()
	{
		return $this->redirect(['/user']);
	}

	/**
	 * @return string|\yii\web\Response
	 */
	public function actionLogin()
	{
		$this->layout = 'small';
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render(
				'login',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * @return \yii\web\Response
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * @return string|\yii\web\Response
	 */
	public function actionContact()
	{
		$this->layout = 'small';
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash(
					'success',
					'Thank you for contacting us. We will respond to you as soon as possible.'
				);
			} else {
				Yii::$app->session->setFlash('error', 'There was an error sending email.');
			}

			return $this->refresh();
		} else {
			return $this->render(
				'contact',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * @return string
	 */
	public function actionAbout()
	{
		return $this->render('about');
	}

	/**
	 * @return string|\yii\web\Response
	 */
	public function actionSignup()
	{
		$this->layout = 'small';
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if ($user->sendConformation()) {
					Yii::$app->getSession()->setFlash('email', true);
				}
				return $this->goHome();
//				if (Yii::$app->getUser()->login($user)) {
//					return $this->goHome();
//				}
			}
		}
		$salepoints = SalePoint::find()->all();
		return $this->render(
			'signup',
			[
				'model' => $model,
				'salepoints' => $salepoints,
			]
		);
	}

	/**
	 * @return string|\yii\web\Response
	 */
	public function actionRequestPasswordReset()
	{
		$this->layout = 'small';
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', 'Проверьте почту для продолжения операции.');

				return $this->goHome();
			} else {
				Yii::$app->getSession()->setFlash(
					'error',
					'Извините, мы не можем сбросить пароль для этого адреса'
				);
			}
		}

		return $this->render(
			'requestPasswordResetToken',
			[
				'model' => $model,
			]
		);
	}

	/**
	 * @param $token
	 *
	 * @return string|\yii\web\Response
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'New password was saved.');

			return $this->goHome();
		}

		return $this->render(
			'resetPassword',
			[
				'model' => $model,
			]
		);
	}

	/**
	 * @param $id
	 * @param $key
	 *
	 * Подтверждение почты
	 *
	 * @return \yii\web\Response
	 */
	public function actionConfirm($id, $key)
	{
		$user = User::findOne(['id' => $id, 'auth_key' => $key, 'status' => User::STATUS_NOT_ACTIVE]);
		if ($user) {
			$user->status = User::STATUS_EMAIL_CONFIRMED;
			$user->save();
			Yii::$app->session->setFlash('activate');
		}

		return $this->goHome();
	}

	/**
	 * ПОЛИТИКА ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ
	 * @return string
	 */
	public function actionPolitic() {
		return $this->renderPartial('politic');
	}
}

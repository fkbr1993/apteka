<?php
use common\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\models\User;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
\frontend\assets\FrontAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="header">
	<div class="color-line">
	</div>
	<div class="light-version" id="logo">
        <span>
            <?= ($promo = User::getPrimaryPromo(Yii::$app->getUser()->id)) ? $promo->label : 'Нет промо'?>
        </span>
	</div>
	<nav role="navigation">
		<div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
		<div class="header-add-link">
			<?php // Html::a('Добавить продажу', ['/user/office/products'], ['class' => 'btn btn-success']) ?>
		</div>
		<div class="navbar-right">
			<ul class="nav navbar-nav no-borders">
				<li class="dropdown">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle label-menu-corner" aria-expanded="false">
						<i class="pe-7s-mail"></i>
						<?php $comments = comment\models\Comment::getUnreadMessages(Yii::$app->user->id) ?>
						<span class="label label-success"><?= count($comments) ?></span>
					</a>
					<ul class="dropdown-menu hdropdown animated flipInX">
						<div class="title">
							У вас <?= count($comments) ?> новых сообщений
						</div>
						<?php foreach ($comments as $comment) : ?>
							<li>
								<a href="#">
									<?= $comment->label ?>
								</a>
							</li>
						<?php endforeach ?>
						<li class="summary">
							<a href="<?= Url::to(['/user/comment']) ?>">Все сообщения</a>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="<?= Url::to(['/site/logout']) ?>" data-method="post">
						<i class="pe-7s-upload pe-rotate-90"></i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</div>
<aside id="menu">
	<div id="navigation">
		<?php if (!Yii::$app->user->isGuest) : ?>
			<div class="profile-picture">
				<a href="/">
					<img alt="logo" class="img-circle m-b" src="<?= User::avatar() ?>">
				</a>

				<div class="stats-label text-color">
					<span class="font-extra-bold font-uppercase"><?= Yii::$app->user->identity->username ?></span>

					<div class="dropdown">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<small class="text-muted">Настройки
								<b class="caret"></b>
							</small>
						</a>
						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li>
								<a href="<?= Url::toRoute(['/user/office/edit']) ?>">Настройки</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?= Url::to(['/site/logout']) ?>" data-method="post">Выход</a>
							</li>
						</ul>
					</div>
					<div>
						<h4 class="font-extra-bold m-b-xs">
							<?= Yii::$app->user->isGuest ? 0 : Yii::$app->user->identity->ball ?>
						</h4>
						<small class="text-muted">Бонусные баллы</small>
					</div>
				</div>
			</div>
		<?php endif ?>
		<ul id="side-menu" class="nav">
			<li>
				<a href="<?= Url::to(['/user']) ?>">
					<span class="nav-label">Личный кабинет</span>
				</a>
			</li>
			<li>
				<a href="<?= Url::to(['/user/sales']) ?>">
					<span class="nav-label">Продажи</span>
				</a>
			</li>
			<li>
				<a href="<?= Url::to(['/user/comment']) ?>">
					<span class="nav-label">Сообщения</span>
				</a>
			</li>
		</ul>
	</div>
</aside>

<div id="wrapper">
	<?php if (isset($this->params['breadcrumbs'])) : ?>
		<div class="normalheader transition animated fadeIn">
			<div class="hpanel">
				<div class="panel-body">
					<h2 class="font-light m-b-xs">
						<?= end($this->params['breadcrumbs']) ?>
					</h2>
					<?php if (Yii::$app->controller->id == 'office' && Yii::$app->controller->action->id == 'index') : ?>
						<small>Основная информация</small>
					<?php endif ?>
				</div>
			</div>
		</div>
	<?php endif ?>
	<div class="content animate-panel clearfix">
		<?= $content ?>
	</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

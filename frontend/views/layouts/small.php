<?php
use common\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\models\User;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="blank">
<div class="color-line"></div>
<?php $this->beginBody() ?>

<?= $content ?>

<?php if (Yii::$app->session->getFlash('activate')) : ?>
	<!-- Modal -->
	<div class="modal fade in" id="activate-modal" style="display: block">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" data-target="#activate-modal" aria-hidden="false">&times;</button>
					<h4 class="modal-title">Ваши данные находятся в обработке, пожалйста подождите, пока администратор их не проверит</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" data-target="#activate-modal">
						Закрыть
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
<?php endif ?>


<?php if (Yii::$app->session->getFlash('email')) : ?>
	<!-- Modal -->
	<div class="modal fade in" id="email-modal" style="display: block">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" data-target="#email-modal" aria-hidden="false">&times;</button>
					<h4 class="modal-title">Вы успешно зарегестрировались. Проверьте пожалуйста свой почтовый ящик для активации письма</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" data-target="#email-modal">
						Закрыть
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
<?php endif ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

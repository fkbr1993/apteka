<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-container">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center m-b-md">
				<h3><?= Html::encode($this->title) ?></h3>
			</div>
			<div class="hpanel">
				<div class="panel-body">
					<?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
					<?= $form->field($model, 'password')->passwordInput() ?>
					<div class="form-group">
						<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Запрос на сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-container">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center m-b-md">
				<h3><?= Html::encode($this->title) ?></h3>
			</div>
			<div class="hpanel">
				<div class="panel-body">
					<p>Пожалуйста введите email. Ссылка на востановление пароля будет отправлена по этому адресу.</p>

					<?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
					<?= $form->field($model, 'email') ?>
					<div class="form-group">
						<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-block']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

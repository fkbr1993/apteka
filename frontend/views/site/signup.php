<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-container">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center m-b-md">
				<h3><?= $this->title ?></h3>
			</div>
			<div class="hpanel">
				<div class="panel-body">
					<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
					<?= $form->field($model, 'username') ?>
					<?= $form->field($model, 'email') ?>
					<?= $form->field($model, 'password')->passwordInput() ?>
					<?= $form->field($model, 'phone') ?>
					<?= $form->field($model, 'salepoint')->widget(
						Select2::classname(),
						[
							'data' => \yii\helpers\ArrayHelper::map($salepoints, 'id', 'address'),
							'options' => [
								'placeholder' => 'Выберите место работы, адрес организации',
								'multiple' => false
							],
							'language' => 'ru',
							'pluginOptions' => [
								'allowClear' => true,
								'tags' => true,
							],
						]
					) ?>
					<?= $form->field($model, 'confirm')->checkbox() ?>
					<p class="m-b-xs">
						<?= Html::a('Агенский договор', '/files/Агенский_договор_общий.doc') ?>
					</p>
					<p>
						<?= Html::a('Политика обработки публичных данных', ['/site/politic'], ['target' => '_newWindow', 'class' => 'user-politic-signup']) ?>
					</p>
					<div class="form-group m-t">
						<?= Html::submitButton(
							'Зарегистрироваться',
							['class' => 'btn btn-default btn-block', 'name' => 'signup-button']
						) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
	        'class' => 'common\components\WebUser',
	        'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
	    'urlManager' => [
		    'rules' => [
			    '<module:user>' => '<module>/office/index',
			    '<module:user>/<action:\w+>' => '<module>/office/<action>',
			    '<module:user>/<action:\w+>/<id:\d+>' => '<module>/office/<action>',
		    ],
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
	'aliases' => [
		'configuration' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'configuration'
		),
		'user' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'user'
		),
		'product' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'product'
		),
		'sale' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'sale'
		),
		'comment' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'comment'
		),
		'salepoint' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'salepoint'
		),
		'promo' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'promo'
		),
		'discount' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'discount'
		),
	],
	'modules' => [
		'user' => [
			'class' => 'user\UserModule'
		],
		'product' => [
			'class' => 'product\ProductModule'
		],
		'sale' => [
			'class' => 'sale\SaleModule'
		],
		'comment' => [
			'class' => 'comment\CommentModule'
		],
		'salepoint' => [
			'class' => 'salepoint\SalePointModule'
		],
		'promo' => [
			'class' => 'promo\PromoModule'
		],
		'discount' => [
			'class' => 'discount\DiscountModule'
		]
	],
    'params' => $params,
];

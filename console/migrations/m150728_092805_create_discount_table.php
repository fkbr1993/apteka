<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150728_092805_create_discount_table
 */
class m150728_092805_create_discount_table extends Migration
{
    public $tableName = '{{%discount}}';

    // Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
		$tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
		$this->createTable(
			$this->tableName,
			[
				'id' => Schema::TYPE_PK,
				'promo_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Промо-код"',
				'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукт"',
				'discount' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Cкидка"',
			],
			$tableOptions
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}

}

<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150728_092755_alter_promo_table
 */
class m150728_092755_alter_promo_table extends Migration
{
	public $tableName = '{{%promo}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $this->dropColumn($this->tableName, 'discount');
    }
    
    public function safeDown()
    {
	    $this->addColumn($this->tableName, 'discount', Schema::TYPE_STRING);
    }

}

<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150729_082509_alter_promo_table
 */
class m150729_082509_alter_promo_table extends Migration
{
	public $tableName = '{{%promo}}';

	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
		$this->addColumn($this->tableName, 'description', Schema::TYPE_TEXT);
	}

	public function safeDown()
	{
		$this->dropColumn($this->tableName, 'description');
	}
}

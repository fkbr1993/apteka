<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_112938_create_sale_table extends Migration
{
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    $this->createTable(
		    'sale',
		    [
			    'id' => Schema::TYPE_PK,
			    'user_id' => Schema::TYPE_INTEGER,
			    'product_id' => Schema::TYPE_INTEGER,
			    'amount' => Schema::TYPE_INTEGER,
			    'salepoint_id' => Schema::TYPE_INTEGER,
			    'ball' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
		    ],$tableOptions
	    );
    }

    public function down()
    {
		$this->dropTable('sale');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150924_115536_alter_sale_table
 */
class m150924_115536_alter_sale_table extends Migration
{
    public function up()
    {
		$this->addColumn('{{%sale}}', 'status', Schema::TYPE_SMALLINT);
    }

    public function down()
    {
	    $this->dropColumn('{{%sale}}', 'status');
    }
}

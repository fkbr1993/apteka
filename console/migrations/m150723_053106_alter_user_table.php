<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150723_053106_alter_user_table
 */
class m150723_053106_alter_user_table extends Migration
{
	public function up()
	{
		$this->dropColumn('user', 'promo');
	}

	public function down()
	{
		$this->addColumn('user', 'promo', Schema::TYPE_STRING);
	}
}

<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150804_125218_alter_sale_table
 */
class m150804_125218_alter_sale_table extends Migration
{
    public function up()
    {
		$this->addColumn('sale', 'created_at', Schema::TYPE_INTEGER );
    }

    public function down()
    {
		$this->dropColumn('sale', 'created_at');
    }
}

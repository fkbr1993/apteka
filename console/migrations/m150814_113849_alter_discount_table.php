<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_113849_alter_discount_table extends Migration
{
    public function up()
    {
		$this->addColumn('discount', 'ball', Schema::TYPE_INTEGER . ' DEFAULT 0');
    }

    public function down()
    {
	    $this->dropColumn('discount', 'ball');
    }
}

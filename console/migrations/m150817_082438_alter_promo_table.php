<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_082438_alter_promo_table extends Migration
{
	public function up()
	{
		$this->addColumn('promo', 'archive', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('promo', 'archive');
	}
}

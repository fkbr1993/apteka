<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_042404_create_salepoint_table extends Migration
{
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    $this->createTable('salepoint',[
		    'id' => Schema::TYPE_PK,
		    'address' => Schema::TYPE_STRING,
	    ],$tableOptions);
    }

    public function down()
    {
	    $this->dropTable('salepoint');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

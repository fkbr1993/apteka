<?php

use yii\db\Schema;
use yii\db\Migration;

class m150908_090358_alter_sale_table extends Migration
{

	public $tableName = '{{%sale}}';

	public function up()
	{
		$this->addColumn($this->tableName, 'price', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn($this->tableName, 'price');
	}
}

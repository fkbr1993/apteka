<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_062336_add_fk_product_to_sale extends Migration
{
	public $tableName = '{{%product}}';

	public function safeUp()
	{
		$this->addForeignKey(
			'sale_to_product',
			'{{%sale}}',
			'product_id',
			$this->tableName,
			'id'
		);
	}

	public function safeDown()
	{
		$this->truncateTable('{{%sale}}');
		$this->dropForeignKey('sale_to_product', '{{%sale}}');
	}
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_081954_alter_product_table extends Migration
{
	public function up()
	{
		$this->dropColumn('product', 'ball');
	}

	public function down()
	{
		$this->addColumn('product', 'ball', Schema::TYPE_INTEGER);
	}
}

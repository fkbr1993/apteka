<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150828_092152_alter_sale_table
 */
class m150828_092152_alter_sale_table extends Migration
{
	public $tableName = '{{%sale}}';

    public function up()
    {
		$this->addColumn($this->tableName, 'client_name', Schema::TYPE_STRING);
		$this->addColumn($this->tableName, 'client_last_name', Schema::TYPE_STRING);
    }

    public function down()
    {
       $this->dropColumn($this->tableName, 'client_name');
       $this->dropColumn($this->tableName, 'client_last_name');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

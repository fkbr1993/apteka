<?php

use yii\db\Schema;
use yii\db\Migration;

class m150910_122853_alter_user_table extends Migration
{
	public function up()
	{
		$this->alterColumn('user', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}

	public function down()
	{
		$this->alterColumn('user', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_094954_modify_image_salepoint_table extends Migration
{
    public function up()
    {
		$this->addColumn('salepoint', 'image_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
		$this->dropColumn('salepoint', 'image_id');
    }
}

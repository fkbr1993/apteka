<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_113907_modify_comment_table extends Migration
{
	public function up()
	{
		$this->addColumn('comment', 'status', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('comment', 'status');
	}
}

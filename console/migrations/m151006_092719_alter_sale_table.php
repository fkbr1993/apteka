<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_092719_alter_sale_table extends Migration
{
	public function up()
	{
		$this->addColumn('sale', 'order_id', Schema::TYPE_INTEGER);
	}

	public function down()
	{
		$this->dropColumn('sale', 'order_id');
	}

}

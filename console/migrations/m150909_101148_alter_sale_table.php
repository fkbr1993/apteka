<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_101148_alter_sale_table extends Migration
{
	public function up()
	{
		$this->alterColumn('sale', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}

	public function down()
	{
		$this->alterColumn('sale', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}
}

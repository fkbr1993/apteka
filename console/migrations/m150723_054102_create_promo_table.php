<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150723_054102_create_promo_table
 */
class m150723_054102_create_promo_table extends Migration
{
	public $tableName = '{{%promo}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    $this->createTable(
		    $this->tableName,
		    [
			    'id' => Schema::TYPE_PK,
			    'user_id' => Schema::TYPE_INTEGER,
			    'label' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Промо-код"',
			    'discount' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Cкидка"',
			    'date_from' => Schema::TYPE_DATETIME . ' NOT NULL COMMENT "Начало скидки"',
			    'date_to' => Schema::TYPE_DATETIME . ' NOT NULL COMMENT "Конец скидки"',
		    ],
		    $tableOptions
	    );
    }
    
    public function safeDown()
    {
	    $this->dropTable($this->tableName);
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_092527_modify_user_table extends Migration
{
    public function up()
    {
	    $this->addColumn('user', 'passport', Schema::TYPE_STRING);
	    $this->addColumn('user', 'registration', Schema::TYPE_STRING);
	    $this->addColumn('user', 'inn', Schema::TYPE_STRING);
	    $this->alterColumn('user', 'ball', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
	    $this->dropColumn('user', 'passport');
	    $this->dropColumn('user', 'registration');
	    $this->dropColumn('user', 'inn');
    }
}

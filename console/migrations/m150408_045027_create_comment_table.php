<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_045027_create_comment_table extends Migration
{
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
		$this->createTable('comment',[
			'id' => Schema::TYPE_PK,
			'user_id' => Schema::TYPE_INTEGER,
			'type' => 'tinyint(1) NOT NULL DEFAULT 0',
			'content' => Schema::TYPE_TEXT,
			'label' => Schema::TYPE_STRING,
		],$tableOptions);
    }

    public function down()
    {
		$this->dropTable('comment');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_114126_modify_user_table extends Migration
{
	public function up()
	{
		$this->addColumn('user', 'first_name', Schema::TYPE_STRING);
		$this->addColumn('user', 'last_name', Schema::TYPE_STRING);
		$this->addColumn('user', 'middle_name', Schema::TYPE_STRING);
		$this->addColumn('user', 'salepoint_id', Schema::TYPE_STRING);
	}

	public function down()
	{
		$this->dropColumn('user', 'first_name');
		$this->dropColumn('user', 'last_name');
		$this->dropColumn('user', 'middle_name');
		$this->dropColumn('user', 'salepoint_id');
	}
}

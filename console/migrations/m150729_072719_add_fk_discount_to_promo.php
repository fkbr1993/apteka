<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150729_072719_add_fk_discount_to_promo
 */
class m150729_072719_add_fk_discount_to_promo extends Migration
{
	public $tableName = '{{%discount}}';

	public function safeUp()
	{
		$this->addForeignKey(
			'discount_to_promo',
			$this->tableName,
			'promo_id',
			'{{%promo}}',
			'id'
		);
	}

	public function safeDown()
	{
		$this->dropForeignKey('discount_to_promo', $this->tableName);
	}
}

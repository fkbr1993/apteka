<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_043616_alter_product_table extends Migration
{
	public function up()
	{
		$this->addColumn('product', 'pack', Schema::TYPE_INTEGER);
	}

	public function down()
	{
		$this->dropColumn('product', 'pack');
	}
}

<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150723_054737_add_fk_promo_to_user_table
 */
class m150723_054737_add_fk_promo_to_user_table extends Migration
{
    public $tableName = '{{%promo}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $this->addForeignKey(
		    'promo_to_user',
		    $this->tableName,
		    'user_id',
		    '{{%user}}',
		    'id',
			'CASCADE',
			'CASCADE'
	    );
    }
    
    public function safeDown()
    {
	    $this->dropForeignKey('promo_to_user', $this->tableName);
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150908_105306_alter_discount_table extends Migration
{
	public function up()
	{
		$this->alterColumn('discount', 'discount', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
		$this->alterColumn('discount', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}

	public function down()
	{
		$this->alterColumn('discount', 'discount', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
		$this->alterColumn('discount', 'ball', Schema::TYPE_DECIMAL . '(14,2) DEFAULT 0');
	}
}

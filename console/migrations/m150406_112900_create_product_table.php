<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_112900_create_product_table extends Migration
{
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    $this->createTable(
		    'product',
		    [
			    'id' => Schema::TYPE_PK,
			    'label' => Schema::TYPE_STRING . ' NOT NULL',
			    'description' => Schema::TYPE_TEXT,
			    'price' => Schema::TYPE_DECIMAL. '(14,2)',
			    'ball' => Schema::TYPE_INTEGER,
			    'image_id' => Schema::TYPE_INTEGER,
		    ], $tableOptions
	    );
    }

    public function down()
    {
	    $this->dropTable('product');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

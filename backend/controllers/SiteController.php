<?php
namespace backend\controllers;

use discount\models\Discount;
use product\models\Product;
use promo\models\Promo;
use sale\models\Sale;
use salepoint\models\SalePoint;
use Yii;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\SignupForm;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'add-sale', 'get-promos', 'get-sale-points', 'get-products',],
						'allow' => true,
					],
					[
						'actions' => ['logout', 'index', 'office', 'error'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionLogin()
	{
		$this->layout = 'small';

		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
			return $this->goBack();
		} else {
			return $this->render(
				'login',
				[
					'model' => $model,
				]
			);
		}
	}

//	public function actionSignup()
//	{
//		$this->layout = 'small';
//
//		$model = new SignupForm();
//		if ($model->load(Yii::$app->request->post())) {
//			if ($user = $model->signup()) {
//				if (Yii::$app->getUser()->login($user)) {
//					return $this->goHome();
//				}
//			}
//		}
//
//		return $this->render(
//			'signup',
//			[
//				'model' => $model,
//			]
//		);
//	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionAddSale($userId, $productId)
	{
		$product = Product::findOne((int)$_GET['productId']);
		$amount = !empty($_GET['amount']) ? $_GET['amount'] : 1;
		$totalBall = 0;
		$totalPrice = 0;
		if ($product) {
			$ball = 0;
			$promo = Promo::findOne(['archive' => 0, 'user_id' => (int)$_GET['userId']]);
			$discounts = $promo ? $promo->discounts : [];
			if ($discount = Discount::findProductDiscount($discounts, $product->id)) {
				$ball = $discount->ball ? $discount->ball : 0;
				$totalPrice = ($product->price - $discount->discount * ($product->price) / 100) * $amount;
			} else {
				$totalPrice = $product->price * $amount;
			}
			$totalBall = ($totalPrice / 100) * $ball;
		}
		$model = new Sale();
		$model->user_id = (int)$_GET['userId'];
		$model->product_id = (int)$_GET['productId'];
		$model->client_name = isset($_GET['name']) ? $_GET['name'] : '';
		$model->client_last_name = isset($_GET['last_name']) ? $_GET['last_name'] : '';
		$model->salepoint_id = isset($_GET['salepointId']) ? $_GET['salepointId'] : 1;
		$model->price = $totalPrice;
		$model->status = (int)isset($_GET['status']) ? $_GET['status'] : 0;
		$model->order_id = isset($_GET['number']) ? $_GET['number'] : 0;
		$model->ball = $totalBall;
		$model->created_at = time();
		$model->amount = $amount;

		if ($model->save()) {
			$user = User::findOne($model->user_id);
			if ($user && $model->status == Sale::STATUS_PAID) {
				$user->ball += $totalBall;
				$user->save(false);
			}
			print Json::encode(['result' => 'added']);
		} else {
			print Json::encode(['result' => 'error', $model->getErrors()]);
		}
	}


	/**
	 * Функция API
	 * Выдает промо-коды пользователей
	 *
	 * Cмещение
	 *
	 * @param int $offset
	 *
	 * Лимит
	 * @param int $limit
	 */
	public function actionGetPromos($offset, $limit)
	{
		$promos = \promo\models\Promo::find()->offset($offset)->limit($limit)->all();
		$result = [];
		/* @var $promo \promo\models\Promo */
		foreach ($promos as $promo) {
			$discounts = [];
			foreach ($promo->discounts as $discount) {
				$discounts[$discount->product_id] = $discount->discount;
			};
			$result[$promo->id] = [
				'user_id' => $promo->user_id,
				'label' => $promo->label,
				'description' => $promo->description,
				'discounts' => $discounts,
				'date_from' => $promo->date_from,
				'date_to' => $promo->date_to,
			];
		}
		print Json::encode($result);
	}

	/**
	 * Функция API
	 * Выдает список продуктов
	 *
	 * Cмещение
	 *
	 * @param int $offset
	 *
	 * Лимит
	 * @param int $limit
	 */
	public function actionGetProducts($offset, $limit)
	{
		$products = \product\models\Product::find()->offset($offset)->limit($limit)->all();
		$result = [];
		/* @var $user \product\models\Product */
		foreach ($products as $product) {
			$result[$product->id] = $product->label;
		}
		print Json::encode($result);
	}

	/**
	 * Функция API
	 * Выдает точки продаж
	 *
	 * Cмещение
	 *
	 * @param int $offset
	 *
	 * Лимит
	 * @param int $limit
	 */
	public function actionGetSalePoints($offset, $limit)
	{
		$salePoints = SalePoint::find()->offset($offset)->limit($limit)->all();
		$result = [];
		/* @var $salePoint SalePoint */
		foreach ($salePoints as $salePoint) {
			$result[$salePoint->id] = $salePoint->address;
		}
		print Json::encode($result);
	}
}

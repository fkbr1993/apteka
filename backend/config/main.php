<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-backend',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'backend\controllers',
	'bootstrap' => ['log'],
	'aliases' => [
		'configuration' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'configuration'
		),
		'user' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'user'
		),
		'product' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'product'
		),
		'sale' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'sale'
		),
		'comment' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'comment'
		),
		'salepoint' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'salepoint'
		),
		'promo' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'promo'
		),
		'discount' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'discount'
		),
	],
	'modules' => [
		'gridview' =>  [
			'class' => '\kartik\grid\Module',
			// enter optional module parameters below - only if you need to
			// use your own export download action or custom translation
			// message source
			 'downloadAction' => 'gridview/export/download',
			// 'i18n' => []
		],
		'user' => [
			'class' => 'user\UserModule'
		],
		'product' => [
			'class' => 'product\ProductModule'
		],
		'sale' => [
			'class' => 'sale\SaleModule'
		],
		'comment' => [
			'class' => 'comment\CommentModule'
		],
		'salepoint' => [
			'class' => 'salepoint\SalePointModule'
		],
		'promo' => [
			'class' => 'promo\PromoModule'
		],
		'discount' => [
			'class' => 'discount\DiscountModule'
		],
	],
	'components' => [
		'user' => [
			'class' => 'common\components\WebUser',
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
		],
		'urlManager' => [
			'rules' => [
				'/' => 'user/user/new_users',
				'<module:comment>' => '<module>/comment/index',
				'<module:comment>/<action:\w+>' => '<module>/comment/<action>',
				'<module:comment>/<action:\w+>/<id:\d+>' => '<module>/comment/<action>',
				'<module:product>' => '<module>/product/index',
				'<module:product>/<action:\w+>' => '<module>/product/<action>',
				'<module:product>/<action:\w+>/<id:\d+>' => '<module>/product/<action>',
				'<module:sale>' => '<module>/sale/index',
				'<module:sale>/<action:\w+>' => '<module>/sale/<action>',
				'<module:sale>/<action:\w+>/<id:\d+>' => '<module>/sale/<action>',
				'<module:salepoint>' => '<module>/salepoint/index',
				'<module:salepoint>/<action:\w+>' => '<module>/salepoint/<action>',
				'<module:salepoint>/<action:\w+>/<id:\d+>' => '<module>/salepoint/<action>',
				'<module:user>' => '<module>/user/index',
				'<module:user>/<action:\w+>' => '<module>/user/<action>',
				'<module:user>/<action:\w+>/<id:\d+>' => '<module>/user/<action>',
				'<module:promo>' => '<module>/promo/index',
				'<module:promo>/<action:\w+>' => '<module>/promo/<action>',
				'<module:promo>/<action:\w+>/<id:\d+>' => '<module>/promo/<action>',
			],
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
	],
	'params' => $params,
];

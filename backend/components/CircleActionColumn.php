<?php
namespace backend\components;

use Yii;
use yii\helpers\Html;

/**
 * Class CircleActionColumn
 *
 * @package backend\components
 */
class CircleActionColumn extends \yii\grid\ActionColumn
{
	/**
	 * Initializes the default button rendering callbacks.
	 */
	protected function initDefaultButtons()
	{
		if (!isset($this->buttons['view'])) {
			$this->buttons['view'] = function ($url, $model, $key) {
				$options = array_merge(
					[
						'title' => Yii::t('yii', 'View'),
						'aria-label' => Yii::t('yii', 'View'),
						'data-pjax' => '0',
						'class' => 'btn btn-xs btn-default',
					],
					$this->buttonOptions
				);

				return '<div class="btn-group">'.Html::a('<i class="fa fa-eye"></i>', $url, $options);
			};
		}
		if (!isset($this->buttons['update'])) {
			$this->buttons['update'] = function ($url, $model, $key) {
				$options = array_merge(
					[
						'title' => Yii::t('yii', 'Update'),
						'aria-label' => Yii::t('yii', 'Update'),
						'data-pjax' => '0',
						'class' => 'btn btn-xs btn-default'
					],
					$this->buttonOptions
				);

				return Html::a('<i class="fa fa-pencil"></i>', $url, $options);
			};
		}
		if (!isset($this->buttons['delete'])) {
			$this->buttons['delete'] = function ($url, $model, $key) {
				$reflect = new \ReflectionClass($model);
				$reflect = $reflect->getShortName();
				if ($reflect == 'Promo') {
					$title = 'В архив';
					$confirm = 'Вы уверены, что хотите отправить промо-код в архив?';
				} else {
					$title = Yii::t('yii', 'Delete');
					$confirm = Yii::t('yii', 'Are you sure you want to delete this item?');
				}
				$options = array_merge(
					[
						'title' => $title,
						'aria-label' => $title,
						'data-confirm' => $confirm,
						'data-method' => 'post',
						'data-pjax' => '0',
						'class' => 'btn btn-xs btn-default',
					],
					$this->buttonOptions
				);

				return Html::a('<i class="fa fa-remove"></i>', $url, $options).'</div>';
			};
		}
	}
}

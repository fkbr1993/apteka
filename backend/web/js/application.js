/**
 * Created by alfred on 28.07.15.
 */

// Upgrade for JSON.stringify, updated to allow arrays
(function(){
	// Convert array to object
	var convArrToObj = function(array){
		var thisEleObj = new Object();
		if(typeof array == "object"){
			for(var i in array){
				var thisEle = convArrToObj(array[i]);
				thisEleObj[i] = thisEle;
			}
		}else {
			thisEleObj = array;
		}
		return thisEleObj;
	};
	var oldJSONStringify = JSON.stringify;
	JSON.stringify = function(input){
		if(oldJSONStringify(input) == '[]')
			return oldJSONStringify(convArrToObj(input));
		else
			return oldJSONStringify(input);
	};
})();

$(document).ready(function(){

	/**
	 * Расставляем изначальные значения скидок
	 */
	$('.di').each(function(){
		$(this).val($(this).data('value'));
	});

	/**
	 * Обработчик ввода скидки в редактировании промокода
	 */
	$('input.di').on('keyup', function(e){
		var key = 1*$(this).val();
		if (isNaN(key))
			$(this).val($(this).val().substr(0, $(this).val().length - 1))
	});

	/**
	 * Обработчик изменения скидки в редактировании промокода
	 */
	//$('input.di').on('change', function(){
	//	var value = $(this).val();
	//	console.log('value:');
	//	console.log(value);
	//
	//	var values = $('.hidden-discounts-values').val();
	//	console.log('values:');
	//	console.log(values);
	//
	//	console.log(typeof(values));
	//
	//	var productId = "product-" + $(this).data('product-id').toString();
	//	console.log('productId:');
	//	console.log(productId);
	//
	//	if (values.length > 0)
	//		values = $.parseJSON( values );
	//	else
	//		values = [];
	//	console.log('values parsed from JSON:');
	//	console.log(values);
	//
	//
	//	console.log('new value will be pushed to values:');
	//	console.debug(values);
	//	console.debug(productId);
	//	console.debug(value);
	//	console.debug(typeof (value));
	//	values[productId] = value;
	//	console.log('new value pushed to values:');
	//	console.log(values);
	//
	//	values = JSON.stringify( values );
	//	console.log('values serialiazed to JSON:');
	//	console.log(values);
	//
	//	$('.hidden-discounts-values').val(values);
	//
	//});

	/**
	 * Обработчик изменения скидки в редактировании промокода
	 */
	$('.promo-form form').on('beforeSubmit', function() {
		var values = [],
			inputs = $('.promo-inputs input');
		$.each(inputs, function() {
			var input = $(this);
			if (input.val() != input.data('value') || input.val() != 0) {
				var index = 'product-' + input.data('product-id');
				if (typeof values[index] == 'undefined') {
					values[index] = {};
				}

				if (input.hasClass('discount')) {
					values[index].discount = input.val();
				} else {
					values[index].ball = input.val();
				}
			}
		});
		values = JSON.stringify(values);
		console.log(values);
		$('.hidden-discounts-values').val(values);
	});

	$('.old .user-submit').on('click', function(e){
		e.preventDefault();
		if ($('.old #user-password').val().length > 0 ){
			if (confirm('Вы действительно хотите изменить пароль?')){
				$('.old form').submit()
			}
			return false
		}
		$('.old form').submit();
	});

});

<?php

namespace user\controllers;

use Faker\Factory;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use user\models\User;
use user\models\SearchUser;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User as CommonUser;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['view', 'create', 'update', 'delete', 'index', 'new_users', 'approve', 'reject'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionFake()
	{
		if (CommonUser::isAdmin()) {
			$faker = Factory::create();
			for ($i = 0; $i < 10; $i++) {
				$model = new CommonUser;
				$model->username = $faker->userName;
				$model->status = 1;
				$model->setPassword('test');
				$model->created_at = time();
				$model->updated_at = time();
				$model->save();
			}
		}

		$this->redirect(['/user/user']);
	}

	/**
	 * Lists all User models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SearchUser();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Displays a single User model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
			]
		);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new User();

		if ($model->load(Yii::$app->request->post())) {
			$model->auth_key = Yii::$app->security->generateRandomString();
			$model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
			$model->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
			$model->created_at = time();
			$model->updated_at = time();
			if ($model->save()) {
				\Yii::$app->getSession()->setFlash(
					'user-view',
					'Переход на создание промо-кода: <br> <a href="/promo/promo/create?user_id=' . $model->id . '">ссылка</a>'
				);

				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

		return $this->render(
			'create',
			[
				'model' => $model,
			]
		);
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$file = $model->image_id;
		if (!empty($model->password)) {
		}
		//var_dump(Yii::$app->request->post(),$model->password,!empty($model->password));exit;
		if ($model->load(Yii::$app->request->post())) {
			if (!empty($model->password)) {
				$model->password_hash = Yii::$app->security->generatePasswordHash(trim($model->password));
				$model->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
			}
			if ($model->save()) {
				FPM::deleteFile($file);
				if (!empty($model->password)) {
					\Yii::$app->getSession()->setFlash(
						'user-view',
						'Пароль изменен успешно! <br>Новый пароль: ' . $model->password
					);
				}
				$model->password = null;
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

		return $this->render(
			'update',
			[
				'model' => $model,
			]
		);
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$user = $this->findModel($id);
		if ($user->status == User::STATUS_ACTIVE || $user->status == User::STATUS_USER) {
			$user->status = User::STATUS_DELETED;
			$user->save();
		} else {
			$user->delete();
		}

		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


	/**
	 * Выводим новых пользователей.
	 *
	 * @return mixed
	 */
	public function actionNew_users()
	{
		$searchModel = new SearchUser();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

		return $this->render(
			'new_users',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Одобряем регистрацию пользователя и меняем ему статус
	 *
	 * @return mixed
	 */
	public function actionApprove($id)
	{
		$model = User::findOne(['id' => $id, 'status' => [User::STATUS_EMAIL_CONFIRMED, User::STATUS_NOT_ACTIVE]]);

		if ($model && $model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->status = User::STATUS_USER;
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			return $this->render(
				'update',
				[
					'model' => $model,
				]
			);
		}

		return $this->redirect(Yii::$app->request->referrer);
	}


	/**
	 * Отказываем пользователю в регистрации и меняем ему статус на удаленного
	 *
	 * @return mixed
	 */
	public function actionReject($id)
	{
		$user = User::findOne(['id' => $id, 'status' => [User::STATUS_EMAIL_CONFIRMED, User::STATUS_NOT_ACTIVE]]);
		if ($user) {
			$user->delete();
		}

		return $this->redirect(Yii::$app->request->referrer);
	}
}

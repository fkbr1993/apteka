<?php
namespace user\models;

use comment\models\Comment;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Comment form
 */
class CommentForm extends Model
{

	/**
	 * @var
	 */
	public $content;

	/**
	 * @var
	 */
	public $label;

	/**
	 * @var
	 */
	public $user_id;

	/**
	 * @var
	 */
	public $type;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['content','label'], 'required'],
			[['content','label'], 'string'],
			['user_id', 'integer', 'skipOnEmpty' => true],
		];
	}

	/**
	 * Добавляем комментарий
	 *
	 * @return bool
	 */
	public function update()
	{
		if ($this->validate()) {
			$comment = new Comment();
			$comment->content = $this->content;
			$comment->user_id = Yii::$app->user->id;
			$comment->type = User::isUser() ? 0 : 1;
			$comment->label = $this->label;
			return $comment->save();
		} else {
			return false;
		}
	}
}

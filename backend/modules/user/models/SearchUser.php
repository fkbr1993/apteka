<?php

namespace user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use user\models\User;

/**
 * SearchUser represents the model behind the search form about `user\models\User`.
 */
class SearchUser extends User
{

	public $timestamp_from;

	public $timestamp_to;


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'status', 'created_at', 'updated_at', 'ball'], 'integer'],
			[
				[
					'username',
					'auth_key',
					'password_hash',
					'password_reset_token',
					'email',
					'phone',
					'salepoint_id',
					'timestamp_from',
					'timestamp_to'
				],
				'safe'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @param boolean $newUser
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $newUser = false)
	{
		$query = User::find()->addOrderBy(['user.id' => 'ASC']);

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
				'sort' => false,
			]
		);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere(
			[
				'status' => $newUser ? [User::STATUS_NOT_ACTIVE, User::STATUS_EMAIL_CONFIRMED] : $this->status,
				'created_at' => $this->created_at,
				'updated_at' => $this->updated_at,
			]
		);

		if ($this->timestamp_from || $this->timestamp_to) {
			$query->select(['user.*, SUM(sale.ball) as sum'])->joinWith(['promo','sale'])->addGroupBy('user.id');

			if ($this->timestamp_from) {
				$from = strtotime(str_replace('/', '.', $this->timestamp_from));
				$query->andFilterWhere(['>=', 'sale.created_at', $from]);
			}
			if ($this->timestamp_to) {
				$to = strtotime(str_replace('/', '.', $this->timestamp_to)) + 86399;
				$query->andFilterWhere(['<=', 'sale.created_at', $to]);
			}
		} else {
			$query->joinWith(['promo']);
		}

		if ((int)$this->username) {
			$query->andFilterWhere(
				[
					'user.id' => (int)$this->username,
				]
			);
		} else {
			$query->andFilterWhere(
				[
					'or',
					['email' => $this->username],
					['like', 'username', $this->username]
				]
			);
		}

		$query->andFilterWhere(['like', 'auth_key', $this->auth_key])
			->andFilterWhere(['like', 'password_hash', $this->password_hash])
			->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'phone', $this->phone])
			->andFilterWhere(['like', 'ball', $this->ball]);

		if ($newUser) {
			$query->andFilterWhere(['like', 'salepoint.address', $this->salepoint_id]);
		}

		return $dataProvider;
	}
}

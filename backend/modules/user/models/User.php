<?php

namespace user\models;

use promo\models\Promo;
use sale\models\Sale;
use Yii;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\ArrayHelper;
use kartik\builder\Form;
use yii\helpers\Html;
use salepoint\models\SalePoint;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $salepoint_id
 * @property string $passport
 * @property string $registration
 * @property string $inn
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{

	/**
	 * Удален
	 */
	const STATUS_DELETED = 0;

	/**
	 * Админ
	 */
	const STATUS_ACTIVE = 10;
	/**
	 *
	 */
	const STATUS_USER = 1;

	/**
	 * Неактивированный, email неподтвержден
	 */
	const STATUS_NOT_ACTIVE = 2;

	/**
	 * Email подтвержден
	 */
	const STATUS_EMAIL_CONFIRMED = 3;

	/**
	 * @var string пароль
	 */
	public $password;

	public $sum;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
	            //'password',
	            'username',
	            'last_name',
	            'auth_key',
	            'passport',
	            'registration',
	            'phone',
	            'password_hash',
	            'email',
	            'created_at',
	            'updated_at',

            ], 'required'],
            [['status', 'created_at', 'updated_at','image_id'], 'integer'],
	        [['email'], 'unique'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'phone',], 'string', 'max' => 255],
	        [[
		        'first_name',
		        'username',
		        'last_name',
		        'middle_name',
		        'salepoint_id',
		        'inn',
		        'passport',
		        'registration',
		        'status',
		        'password',
		        'email',
		        'phone',
		        'created_at',
		        'updated_at',
	        ], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
	        ['ball', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
	        'last_name' => 'Фамилия',
	        'middle_name' => 'Отчество',
	        'salepoint_id' => 'Адрес',
	        'inn' => 'ИНН',
	        'registration' => 'Прописка',
	        'passport' => 'Номер паспорта',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'EMail',
            'status' => 'Статус',
	        'phone' => 'Телефон',
	        'ball' => 'Баллы',
	        'sum' => 'Баллы',
	        'image_id' => 'Аватар',
	        'image_preview' => 'Превью аватара',
            'created_at' => 'Дата регистрации',
            'updated_at' => 'Дата обновления',
	        'password' => $this->isNewRecord ? 'Пароль' : 'Новый пароль',
        ];
    }

	/**
	 * Список статусов
	 * @return array
	 */
	public static function getStatuses(){
		return [
			self::STATUS_NOT_ACTIVE => 'Не подтвержден',
			self::STATUS_EMAIL_CONFIRMED => 'Подтвержден',
			self::STATUS_DELETED => 'Удалён',
			self::STATUS_USER => 'Пользователь',
			self::STATUS_ACTIVE => 'Админ',
		];
	}

	/**
	 * Текущий статус
	 * @return null
	 */
	public function getStatus(){
		return empty(self::getStatuses()[$this->status]) ? null : self::getStatuses()[$this->status];
	}

	/**
	 * Сохраняем аватар пользователя
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => false,
					'required' => false,
				]
			]
		);
	}

	/**
	 * До удаления пользователя удаляем его аватар
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->image_id);

		return true;
	}

	/**
	 * @param bool $viewAction
	 *
	 * Формируем поля для формы
	 * $viewAction = false - редактирование
	 * $viewAction = true - просмотр
	 *
	 * @return array
	 */
	public function prepareForm($viewAction = false)
	{
		return $viewAction ? [
			'id',
			'username',
			'last_name',
			'middle_name',
			'passport',
			'registration',
			'inn',
			'salepoint.address',
			'email',
			'phone',
			'status' => [
				'attribute' => 'status',
				'value' => $this->getStatus(),
			],
			'ball',
			'created_at' => [
				'attribute' => 'created_at',
				'value' => date('d.m.Y h:i', $this->created_at),
			],
			'updated_at' => [
				'attribute' => 'updated_at',
				'value' => date('d.m.Y h:i', $this->updated_at),
			],
			[
				'attribute' => 'image_id',
				'format' => 'raw',
				'value' => $this->image_id
					? Html::img(
						\metalguardian\fileProcessor\helpers\FPM::originalSrc($this->image_id)
					)
					: null,
			],
		] : [
			'username' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'last_name' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'middle_name' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'passport' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'registration' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'inn' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'salepoint_id' => [
				'type' => \kartik\builder\Form::INPUT_DROPDOWN_LIST,
				'items' => ArrayHelper::map(SalePoint::find()->all(), 'id', 'address'),
			],
			'email' => [
				'type' => Form::INPUT_TEXT,
			],
			'status' => [
				'type' => \kartik\builder\Form::INPUT_DROPDOWN_LIST,
				'items' => $this->getStatuses(),
			],
			'phone' => [
				'type' => Form::INPUT_TEXT,
			],
			'ball' => [
				'type' => Form::INPUT_TEXT,
			],
			'password' => [
				'type' => \kartik\builder\Form::INPUT_TEXT,
			],
			'image_preview' => [
				'attribute' => 'image_preview',
				'type' => \kartik\builder\Form::INPUT_RAW,
				'value' => $this->isNewRecord == 'create'
					? null
					: Html::img(
						\metalguardian\fileProcessor\helpers\FPM::src($this->image_id, 'user', 'avatar')
					),
			],
		];
	}

	/**
	 * Возвращаем количество новых пользователей
	 * @return int|string
	 */
	public static function countNewUsers() {
		return self::find()->where(['status' => [self::STATUS_EMAIL_CONFIRMED, self::STATUS_NOT_ACTIVE]])->count();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalepoint()
	{
		return $this->hasOne(SalePoint::className(), ['id' => 'salepoint_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPromo()
	{
		return $this->hasOne(Promo::className(), ['user_id' => 'id'])->addOrderBy(['archive' => 'ASC']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSale()
	{
		return $this->hasMany(Sale::className(), ['user_id' => 'id']);
	}

}

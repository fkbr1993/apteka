<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel user\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новые пользователи';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-8 user-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="hpanel">
		<div class="panel-body">
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>

			<p>Поиск пользователя <small class="pull-right">БетулаФарм<sup>®</sup></small></p>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => '/user/user/new_users',
				]
			) ?>
			<div class="input-group">
				<?= $form->field(
					$searchModel,
					'username',
					[
						'template' => '{input}',

					]
				)->input(
					'text',
					[
						'class' => 'input form-control',
						'placeholder' => 'Имя пользователя или ID'
					]
				) ?>
				<span class="input-group-btn">
						<button class="btn btn btn-success" type="button">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
			</div>
			<?php $form->end() ?>

			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'options' => ['class' => 'table-responsive m-t-lg'],
					'tableOptions' => ['class' => 'table no-footer new_users'],
					'summary' => '',
					'columns' => [
						'id',
						[
							'attribute' => 'username',
							'format' => 'raw',
							'value' => function ($model) {
								return '<a href="' . Url::to(
									['/user/user/view', 'id' => $model->id]
								) . '">' . $model->username . '</a>';
							},
						],
						'email:email',
						'phone',
						[
							'attribute' => 'promo',
							'label' => 'Промокод',
							'value' => function($model)  {
								return isset($model->promo) ? $model->promo->label : '';
							}
						],
//						'salepoint_id' => [
//							'attribute' => 'salepoint_id',
//							'value' => function ($model) {
//								return $model->salepoint_id ? $model->salepoint->address : '';
//							},
//						],
						// 'status',
						// 'created_at',
						// 'updated_at',
						[
							'format' => 'raw',
							'value' => function ($model) {
								$buttons = '<div class="btn-group">';
								$buttons .= Html::a(
									'Принять',
									['/user/user/approve', 'id' => $model->id],
									['title' => 'Принять', 'class' => 'btn btn-default btn-xs']
								);
								$buttons .= Html::a(
									'Отклонить',
									['/user/user/reject', 'id' => $model->id],
									['title' => 'Отклонить', 'class' => 'btn btn-default btn-xs', 'data' => ['confirm' => 'Вы уверены, что хотите отказать пользователю в регистрации?']]
								);
								$buttons .= '</div>';
								return $buttons;
							}
						],
					],
				]
			); ?>
		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model user\models\User */
/* @var $form yii\widgets\ActiveForm */

$newRecordClass = $model->isNewRecord ? 'new' : 'old';
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/application.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="user-form <?= $newRecordClass ?>">

	<?php
	echo Html::errorSummary(
		$model,
		[
			'class' => 'alert alert-danger'
		]
	);
	echo Html::tag('br');

	$form = \kartik\form\ActiveForm::begin(
		[
			'enableClientValidation' => false,
			'options' => ['class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'],
			'fieldConfig' => [
				'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
				'labelOptions' => ['class' => 'col-sm-2 control-label'],
			],
		]
	);

	$rows = $model->prepareForm();

	echo \kartik\builder\Form::widget(
		[
			'model' => $model,
			'form' => $form,
			'columns' => 1,
			'attributes' => $rows,
		]
	);

	if (Yii::$app->controller->action->id != 'approve') {
		echo $form->field($model, 'image_id')->widget(
			\kartik\file\FileInput::classname(),
			[
				'pluginOptions' => [
					'showCaption' => false,
					'showRemove' => false,
					'showUpload' => false,
					'browseClass' => 'btn btn-default btn-md-1',
					'browseIcon' => '',
					'browseLabel' => 'Загрузить фото'
				],
				'options' => ['accept' => 'image/*'],
			]
		)->label('Аватар');
	}
	?>


	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-2">
			<?= Html::resetButton('Сросить', ['class' => 'btn btn-default']) ?>
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary user-submit']) ?>
		</div>
	</div>

	<?php \kartik\form\ActiveForm::end(); ?>

</div>

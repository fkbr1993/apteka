<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model user\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-5 hpanel">
	<?php if (\Yii::$app->getSession()->getFlash('user-view')) { ?>
		<div class="alert alert-success m-b-md">
			<h6><?= \Yii::$app->getSession()->getFlash('user-view'); ?></h6>
		</div>
	<?php } ?>

	<p class="m-b-md">
		<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(
			'Удалить',
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Вы уверены, что хотите удалить?',
					'method' => 'post',
				],
			]
		) ?>
	</p>
	<div class="panel-body">

		<?= DetailView::widget(
			[
				'model' => $model,
				'options' => ['class' => 'table no-footer'],
				'attributes' => $model->prepareForm(true),
			]
//				'id',
//				'username',
//				'auth_key',
//				'password_hash',
//				'password_reset_token',
//				'email:email',
//				'status',
//				'phone',
//				'ball',
//				[
//					'label' => 'Last Auth',
//					'format' => 'date',
//					'value' => $model->last_auth,
//				],
//				'photo_id',
//				[
//					'label' => 'Created At',
//					'format' => 'date',
//					'value' => $model->created_at,
//				],
//				[
//					'label' => 'Updated At',
//					'format' => 'date',
//					'value' => $model->updated_at,
//				],
		) ?>
	</div>
</div>

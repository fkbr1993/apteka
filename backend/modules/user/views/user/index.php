<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use product\models\Product;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel user\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$users = $dataProvider->models;

$gridColumns = [
	'username',
	'last_name',
	'middle_name',
	'salepoint.address',
	'email:email',
	'phone',
	[
		'attribute' => 'ball',
		'format' => 'raw',
		'value' => function ($model) {
			if ($model->sum) {
				return $model->sum;
			} else {
				return $model->ball;
			}
		},
	],
	[
		'format' => 'raw',
		'label' => 'Промокод',
		'value' => function ($model) {
			if (isset($model->promo)) {
				if ($model->promo->archive) {
					$link = '';
				} else {
					$link = $model->promo->label;
				}

				return $link;
			} else {
				return '';
			}
		},
	],
	'passport',
	'registration',
	'inn',
	'created_at' => [
		'attribute' => 'created_at',
		'value' => function ($model) {
			return date('d.m.Y h:i', $model->created_at);
		},
	],
	'updated_at' => [
		'attribute' => 'updated_at',
		'value' => function ($model) {
			return date('d.m.Y h:i', $model->updated_at);
		},
	],
];
?>
<div class="col-lg-8 user-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
		<?= Html::a('Новый пользователь', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<div class="hpanel">
		<div class="panel-body">
			<span class="pull-right total-count">Всего пользоватлей: <?= \user\models\User::find()->count() ?></span>
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>


			<p>Поиск пользователя
				<small class="pull-right">БетулаФарм
					<sup>®</sup>
				</small>
			</p>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => '/user/user/index',
				]
			) ?>
			<div class="input-group">
				<?= $form->field(
					$searchModel,
					'username',
					[
						'template' => '{input}',

					]
				)->input(
					'text',
					[
						'class' => 'input form-control',
						'placeholder' => 'Имя пользователя или ID или e-mail'
					]
				) ?>
				<span class="input-group-btn">
						<button class="btn btn btn-success" type="submit">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
			</div>
			<?php $form->end() ?>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => ['/user/user/index'],
				]
			) ?>
			<div class="form-group m-t-md">
				<p>Поиск по дате продажи</p>
				<div class="input-group">
					<?= DatePicker::widget(
						[
							'model' => $searchModel,
							'attribute' => 'timestamp_from',
							'attribute2' => 'timestamp_to',
							'separator' => 'До',
							'type' => DatePicker::TYPE_RANGE,
							'form' => $form,
							'pluginOptions' => [
								'format' => 'dd/mm/yyyy',
								'autoclose' => true,
							]
						]
					) ?>
					<span class="input-group-btn">
						<button class="btn btn btn-success" type="submit">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
				</div>
			</div>
			<?php $form->end() ?>
			<div class="pull-right m-t-md">
				<?= ExportMenu::widget(
					[
						'dataProvider' => $dataProvider,
						'columns' => $gridColumns,
						'fontAwesome' => true,
						'exportConfig' => [
							ExportMenu::FORMAT_PDF => false
						],
					]
				) ?>
			</div>
			<span class="clearfix"></span>

			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'options' => ['class' => 'table-responsive m-t-lg'],
					'tableOptions' => ['class' => 'table table-striped table-hover no-footer new_users'],
					'summary' => '',
					'columns' => [
						[
							'label' => false,
							'format' => 'raw',
							'contentOptions' => ['class' => 'client-avatar'],
							'value' => function ($model) {
								return Html::img(
									$model->image_id ? \metalguardian\fileProcessor\helpers\FPM::src(
										$model->image_id,
										'user',
										'avatar'
									) : '/css/img/user.png'
								);
							}
						],
						[
							'attribute' => 'username',
							'format' => 'raw',
							'value' => function ($model) {
								return '<a href="' . Url::to(
									['/user/user/view', 'id' => $model->id]
								) . '">' . $model->username . '</a>';
							},
						],
						[
							'label' => false,
							'contentOptions' => ['class' => 'contact-type'],
							'format' => 'raw',
							'value' => function ($model) {
								return '<i class="fa fa-envelope"></i>';
							}
						],
						'email:email',
						[
							'attribute' => 'ball',
							'format' => 'raw',
							'value' => function ($model) {
								if ($model->sum) {
									return $model->sum;
								} else {
									return $model->ball;
								}
							},
						],
						[
							'format' => 'raw',
							'label' => 'Промокод',
							'value' => function ($model) {
								if (isset($model->promo)) {
									if ($model->promo->archive) {
										$link = Html::a(
											'пересоздать',
											Url::to(
												[
													'/promo/create',
													'id' => $model->promo->id,
													'last' => $model->promo->id
												]
											),
											['class' => 'promo']
										);
									} else {
										$link = Html::a(
											$model->promo->label,
											Url::to(['/promo/view', 'id' => $model->promo->id,]),
											['class' => 'promo']
										);
									}


									return $link;
								} else {
									return Html::a(
										'создать',
										Url::to(['/promo/create', 'user_id' => $model->id,]),
										['class' => 'promo']
									);
								}
							},
						],
//						'salepoint_id' => [
//							'attribute' => 'salepoint_id',
//							'value' => function ($model) {
//								return $model->salepoint_id ? $model->salepoint->address : '';
//							},
//						],
						// 'status',
						// 'created_at',
						// 'updated_at',
						['class' => 'backend\components\CircleActionColumn'],
					],
				]
			); ?>
		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>

<?php

namespace product\models;

use discount\models\Discount;
use kartik\builder\Form;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\fileProcessor\behaviors\UploadBehavior;



/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $price
 * @property string $pack
 * @property string $image_id
 */
class Product extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['label'], 'required'],
			[['description'], 'string'],
			[['pack'], 'integer'],
			[['label'], 'string', 'max' => 255],
			['price', 'match', 'pattern' => '/^(?:0|[1-9]\d*)(?:\.\d{2})?$/']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'label' => 'Заголовок',
			'description' => 'Описание',
			'price' => 'Цена',
			'ball' => 'Баллы',
			'pack' => 'Упаковка',
			'image_id' => 'Изображение',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => true,
					'required' => false,
				]
			]
		);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiscount()
	{
		return $this->hasOne(Discount::className(), ['product_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->image_id);

		return true;
	}
}

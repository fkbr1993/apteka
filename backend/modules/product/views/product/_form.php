<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

	<?php
	echo Html::errorSummary(
		$model,
		[
			'class' => 'alert alert-danger'
		]
	);

	$form = \kartik\form\ActiveForm::begin(
		[
			'enableClientValidation' => false,
			'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
			'fieldConfig' => [
				'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
				'labelOptions' => ['class' => 'col-sm-2 control-label'],
			],
		]
	);


	echo $form->field($model, 'label'),
	$form->field($model, 'price'),
	$form->field($model, 'pack'),
	$form->field($model, 'description')->textarea(); ?>
	<?php if (!$model->isNewRecord) : ?>
		<div class="form-group field-product-preview">
			<label for="product-preview" class="col-sm-2 control-label">Превью</label>
			<div class="col-sm-10"><?= $model->image_id ? Html::img(\metalguardian\fileProcessor\helpers\FPM::src(
					$model->image_id,
					'banner',
					'standard'
				)) : '' ?></div>
		</div>
	<?php endif ?>
	<?= $form->field($model, 'image_id')->widget(
		\kartik\file\FileInput::classname(),
		[
			'pluginOptions' => [
				'showCaption' => false,
				'showRemove' => false,
				'showUpload' => false,
				'browseClass' => 'btn btn-default btn-md-1',
				'browseIcon' => '',
				'browseLabel' => 'Загрузить фото'
			],
			'options' => ['accept' => 'image/*'],
		]
	)->label('Изображение');
	?>

	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-2">
			<?= Html::resetButton('Сросить', ['class' => 'btn btn-default']) ?>
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

	<?php \kartik\form\ActiveForm::end(); ?>

</div>

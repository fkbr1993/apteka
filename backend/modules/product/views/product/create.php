<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model product\models\Product */

$this->title = 'Новый продукт';
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

	<div class="col-lg-5 hpanel">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model product\models\Product */

$this->title = 'Редактирование продукта: ' . ' ' . $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="product-update">

	<div class="col-lg-5 hpanel">
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

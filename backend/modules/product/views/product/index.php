<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel product\models\SearchProduct */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция';
$this->params['breadcrumbs'][] = $this->title;
$colors = [
	'hgreen', 'hyellow', 'hred', 'hblue', 'hviolet'
]
?>
<p class="m-b-md">
	<?= Html::a('Новый продукт', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<div class="ng-scope">
	<div class="row ng-scope">
		<?php foreach ($dataProvider->getModels() as $model) : ?>
			<div class="col-lg-4">
				<div class="hpanel <?= $colors[rand(0, 4)] ?> contact-panel">
					<div class="panel-body">
						<div class="btn-group pull-right">
							<a aria-label="Редактировать" title="Редактировать" href="<?= Url::toRoute(
								['/product/product/update', 'id' => $model->id]
							) ?>" class="btn btn-xs btn-default">
								<i class="fa fa-pencil"></i>
							</a>
							<a data-method="post" data-confirm="Вы уверены, что хотите удалить этот элемент?" aria-label="Удалить" title="Удалить" href="<?= Url::toRoute(
								['/product/product/delete', 'id' => $model->id]
							) ?>" class="btn btn-xs btn-default">
								<i class="fa fa-remove"></i>
							</a>
						</div>
						<img src="<?= $model->image_id ? \metalguardian\fileProcessor\helpers\FPM::src(
							$model->image_id,
							'banner',
							'standard'
						) : null ?>" class="m-b" alt="logo">
						<h3>
							<a href="<?= \yii\helpers\Url::toRoute(
								['/product/product/view', 'id' => $model->id]
							) ?>"> <?= $model->label ?></a>
						</h3>
						<p class="m-t-xs"><?= mb_strimwidth($model->description, 0, 100, '...', 'utf-8') ?></p>
					</div>
					<div class="panel-footer contact-footer">
						<div class="row">
							<div class="col-md-6 border-right">
								<div class="contact-stat">
									<span>Упаковка: </span>
									<strong><?= $model->pack ?> капсул</strong>
								</div>
							</div>
							<div class="col-md-6">
								<div class="contact-stat">
									<span>Стоимость: </span>
									<strong><?= $model->price ?> <i class="fa fa-rub"></i></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

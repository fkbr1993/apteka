<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel discount\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Discounts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">
	<h2 class="font-light">
		<?= Html::encode($this->title) ?>
	</h2>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
        <?= Html::a('Create Discount', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	    'summary' => '',
        'columns' => [
	        'id',

            'promo_id',
            'product_id',
            'discount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

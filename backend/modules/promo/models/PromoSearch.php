<?php

namespace promo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use promo\models\Promo;

/**
 * PromoSearch represents the model behind the search form about `promo\models\Promo`.
 */
class PromoSearch extends Promo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', ], 'integer'],
            [['label', 'description', 'date_from', 'date_to'], 'safe'],
	        ['user_id', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Promo::find()->Joinwith('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user.username' => $this->user_id,
        ]);
	    if ($this->date_from) {
		    $query->andFilterWhere(['>=', 'date_from', date('Y.m.d h:i:s', strtotime($this->date_from))]);
	    }
	    if ($this->date_to) {
		    $query->andFilterWhere(['<=', 'date_to', date('Y.m.d h:i:s', strtotime($this->date_to))]);
	    }
	    if (Yii::$app->request->get('archive')) {
		    $query->andFilterWhere(
			    [
				    'or',
				    ['archive' => 1],
				    ['archive' => 2],
			    ]
		    );
	    }
        $query->andFilterWhere(['like', 'label', $this->label]);
	    $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}

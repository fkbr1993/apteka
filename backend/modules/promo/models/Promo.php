<?php

namespace promo\models;

use discount\models\Discount;
use user\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Промокод
 *
 * @property integer $id идентификатор
 * @property integer $user_id идентификатор провизора
 * @property string $label сам промокод
 * @property string $date_from дата начала действия скидки
 * @property string $date_to дата конца действия скидки
 *
 * @property \user\models\User $user провизор
 * @property Discount[] $discounts
 */
class Promo extends \yii\db\ActiveRecord
{

	/**
	 * Cкидки промокода
	 *
	 * @var array
	 */
	public $discountsInput;

	public $cache;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'promo';
	}

	/**
	 * @return bool
	 */
	public function beforeSave()
	{
		$this->date_from = date('Y.m.d h:i:s', strtotime($this->date_from));
		$this->date_to = date('Y.m.d h:i:s', strtotime($this->date_to));

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id',], 'integer'],
			[['user_id', 'label', 'date_from', 'date_to'], 'required'],
			[['description',], 'string'],
//			[['label',], 'unique'],
			[['description', 'date_from', 'date_to', 'discountsInput',], 'safe'],
			[['label'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'label' => 'Промо-код',
			'description' => 'Описание',
			'date_from' => 'Начало скидки',
			'date_to' => 'Конец скидки',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(\user\models\User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiscounts()
	{
		return $this->hasMany(Discount::className(), ['promo_id' => 'id']);
	}

	/**
	 * @return string
	 */
	public function getDiscountsInput()
	{
		$discountsArray = [];
		foreach ($this->discounts as $discount) {
			$discountsArray['product-' . $discount->product_id] = $discount->discount;
		}

		return Json::encode($discountsArray);
	}


	/**
	 * Возвращает скидку на конкретный продукт
	 *
	 * @param int $productId идентификатор продукта
	 * @param int $promotId идентификатор промо-кода
	 *
	 * @return int размер скидки в %
	 */
	public function getDiscount($productId, $promoId = 0)
	{
		if (!$this->cache) {
			$condition = ['product_id' => $productId, 'promo_id' => $promoId];
			$promos = Discount::findAll($condition);
		} else {
			$promos = $this->cache;
		}
		$discounts = ArrayHelper::map($promos, 'product_id', 'discount');
		if (!empty($discounts[$productId])) {
			return $discounts[$productId];
		}

		return 0;
	}

	/**
	 * Возвращает баллы для конкретного продукта
	 *
	 * @param int $productId идентификатор продукта
	 * @param int $promotId идентификатор промо-кода
	 *
	 * @return int размер балла в %
	 */
	public function getBall($productId, $promoId = 0) {
		if (!$this->cache) {
			$condition = ['product_id' => $productId, 'promo_id' => $promoId];
			$promos = Discount::findAll($condition);
		} else {
			$promos = $this->cache;
		}
		$discounts = ArrayHelper::map($promos, 'product_id', 'ball');
		if (!empty($discounts[$productId])) {
			return $discounts[$productId];
		}

		return 0;
	}

	/**
	 * @param bool $insert
	 * @param array $changedAttributes
	 */
	public function afterSave($insert, $changedAttributes)
	{
		if (!empty($this->discountsInput)) {
			$this->saveDiscounts();
		}

		return parent::afterSave($insert, $changedAttributes);
	}

	/**
	 * При удалении промокода удаляются и все скидки связанные с ним
	 *
	 * @throws \Exception
	 */
	public function beforeDelete()
	{
		foreach ($this->discounts as $discount) {
			$discount->delete();
		}
	}


	/**
	 * Создаём и обновляем скидки вместе с промокодом
	 */
	protected function saveDiscounts()
	{
		$discounts = Json::decode($this->discountsInput);
		//var_dump($discounts);exit;
		$savedDiscounts = Discount::find()->where(['promo_id' => $this->id,])->all();
		$savedDiscountsArray = [];
		foreach ($savedDiscounts as $savedDiscount) {
			$savedDiscountsArray[$savedDiscount->product_id] = $savedDiscount;
		}
		foreach ($discounts as $product => $value) {
			$productId = str_replace('product-', '', $product);
			if (!array_key_exists($productId, $savedDiscountsArray)) {//если еще нет скидки для этой пары промо-продукт
				$model = new Discount();
				$model->promo_id = $this->id;
				$model->product_id = $productId;
				$model->discount = isset($value['discount']) ? $value['discount'] : 0;
				$model->ball = isset($value['ball']) ? $value['ball'] : 0;
				$model->save();
			} elseif (isset($value['discount']) && $value['discount'] != $savedDiscountsArray[$productId]->discount ||
				isset($value['ball']) && $value['ball'] != $savedDiscountsArray[$productId]->ball) {//если скидка уже есть, но имеет другое значение
				$model = $savedDiscountsArray[$productId];
				if (isset($value['discount']) && $value['discount'] != $savedDiscountsArray[$productId]->discount) {
					$model->discount = $value['discount'];
				}
				if (isset($value['ball']) && $value['ball'] != $savedDiscountsArray[$productId]->ball) {
					$model->ball = $value['ball'];
				}
				$model->save();
			}
		}
	}

	/**
	 * id пользователя
	 *
	 * @param $id
	 *
	 * Генерируем промо-код
	 *
	 * @return string
	 */
	public static function generate()
	{
		$uniq = false;

		while ($uniq == false) {
			$code = 'PROMO';
			for ($i = 0; $i < 6; $i++) {
				$code .= rand(0, 9);
			}

			if (!self::findOne(['label' => $code])) {
				$uniq = true;
			}
		}

		return $code;
	}
}

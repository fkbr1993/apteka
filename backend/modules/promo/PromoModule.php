<?php

namespace promo;

/**
 * Class PromoModule
 *
 * Промокоды дающие скидку и фиксирующие провизора от которого пришел покупатель,
 * создаются на определенный срок
 *
 * @package promo
 */
class PromoModule extends \yii\base\Module
{
    public $controllerNamespace = 'promo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

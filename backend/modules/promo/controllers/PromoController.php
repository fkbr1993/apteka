<?php

namespace promo\controllers;

use discount\models\Discount;
use product\models\Product;
use Yii;
use promo\models\Promo;
use promo\models\PromoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PromoController implements the CRUD actions for Promo model.
 */
class PromoController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['view', 'create', 'update', 'delete', 'index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Promo models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PromoSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Displays a single Promo model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
			]
		);
	}

	/**
	 * Creates a new Promo model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Promo();
		$products = Product::find()->all();
		if (!empty($_GET['user_id'])) {
			$model->user_id = 1 * $_GET['user_id'];
		}
		$last = Yii::$app->request->get('last');
		$promo = 0;
		if (Yii::$app->request->isGet) {
			if ($last) {
				$promo = Promo::findOne($last);
				if ($promo) {
					$model->user_id = $promo->user_id;
					$model->label = $promo->label;
					$model->description = $promo->description;
				} else {
					$model->label = Promo::generate();
				}
			} else {
				$model->label = Promo::generate();
			}
		}
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$promo = Promo::findOne($last);
			if ($promo) {
				$promo->archive = 2;
				$promo->save();
			}
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render(
				'create',
				[
					'model' => $model,
					'products' => $products,
					'promo' => $promo,
				]
			);
		}
	}

	/**
	 * Updates an existing Promo model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$products = Product::find()->all();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render(
				'update',
				[
					'model' => $model,
					'products' => $products,
				]
			);
		}
	}

	/**
	 * Deletes an existing Promo model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->archive = 1;
		$model->save();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Promo model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Promo the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Promo::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model promo\models\Promo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-view">
	<div class="col-lg-5 hpanel">
		<p class="m-b-md">
			<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			<?= Html::a(
				'В архив',
				['delete', 'id' => $model->id],
				[
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => 'Вы уверены, что хотите отправить промо-код в архив?',
						'method' => 'post',
					],
				]
			) ?>
		</p>
		<div class="panel-body">

			<?php
			$fields = [];
			foreach ($model->discounts as $discount) {
				if (!is_null($discount->discount)) {
					$fields[] = [
						'label' => 'Cкидка на продукт "' . $discount->product->label . '"',
						'value' => $discount->discount,
					];
				}
				if (!is_null($discount->ball)) {
					$fields[] = [
						'label' => 'Баллы за продукт "' . $discount->product->label . '"',
						'value' => $discount->ball,
					];
				}
			}

			?>
			<?= DetailView::widget(
				[
					'model' => $model,
					'options' => ['class' => 'table no-footer'],
					'attributes' => \yii\helpers\ArrayHelper::merge(
						[
							'id',
							'user_id',
							'label',
							'description',
							[
								'attribute' => 'date_from',
								'value' => date('d.m.Y', strtotime($model->date_from)),
							],
							[
								'attribute' => 'date_to',
								'value' => date('d.m.Y', strtotime($model->date_to)),
							],
						],
						$fields
					),
				]
			) ?>
		</div>
	</div>
</div>

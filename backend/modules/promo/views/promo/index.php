<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel promo\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо-коды';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-lg-8 promo-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
		<?= Html::a('Новый промо-код', ['create', 'id' => 1, ], ['class' => 'btn btn-success']) ?>
	</p>
	<div class="hpanel">
		<div class="panel-body">
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>

			<p>Поиск промо-кода <small class="pull-right">БетулаФарм<sup>®</sup></small></p>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => '/promo/promo/index',
				]
			) ?>
			<div class="input-group">
				<?= $form->field(
					$searchModel,
					'label',
					[
						'template' => '{input}',

					]
				)->input(
					'text',
					[
						'class' => 'input form-control',
						'placeholder' => 'Промо-код или ID'
					]
				) ?>
				<span class="input-group-btn">
						<button class="btn btn btn-success" type="submit">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
			</div>
			<?php $form->end() ?>

			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'options' => ['class' => 'table-responsive m-t-lg'],
					'summary' => '',
					'tableOptions' => ['class' => 'table table-striped table-hover no-footer'],
					'columns' => [
						[
							'attribute' => 'user_id',
							'format' => 'raw',
							/** @var \promo\models\Promo $model */
							'value' => function ($model) {
								return '<a href="' . Url::to(
									['/user/view', 'id' => $model->user_id]
								) . '">' . $model->user->username . '</a>';
							},
						],
						'label',
						[
							'attribute' => 'date_from',
							'value' => function ($model) {
								return date('d/m/Y', strtotime($model->date_from));
							},
						],
						[
							'attribute' => 'date_to',
							'value' => function ($model) {
								return date('d/m/Y', strtotime($model->date_to));
							},
						],
						['class' => 'backend\components\CircleActionColumn'],
					],
				]
			); ?>

		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>



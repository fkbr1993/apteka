<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use user\models\User;
use kartik\date\DatePicker;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model promo\models\Promo */
/* @var $form yii\widgets\ActiveForm */
/* @var $products array */
/* @var $product product\models\Product */

$this->registerJsFile(
	Yii::$app->request->baseUrl . '/js/application.js',
	[
		'depends' => [\yii\web\JqueryAsset::className()],
		'publishOptions' => ['forceCopy' => true]
	]
);

$users = User::find()->all();

if (!$model->isNewRecord) {
	$model->date_to = date('d.m.Y', strtotime($model->date_to));
	$model->date_from = date('d.m.Y', strtotime($model->date_from));
}

$promoId = 0;

if (isset($promo) && is_object($promo)) {
	$promoId = $promo->id;
}

?>

<div class="promo-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'user_id')->dropDownList(
		\yii\helpers\ArrayHelper::map($users, 'id', 'username')
	) ?>

	<?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

	<?= DatePicker::widget(
		[
			'model' => $model,
			'attribute' => 'date_from',
			'attribute2' => 'date_to',
			'options' => ['placeholder' => 'Начало скидки'],
			'options2' => ['placeholder' => 'Конец скидки'],
			'separator' => 'До',
			'type' => DatePicker::TYPE_RANGE,
			'form' => $form,
			'pluginOptions' => [
				'format' => 'dd.mm.yyyy',
				'autoclose' => true,
			]
		]
	) ?>
</div>

<?= Html::activeHiddenInput(
	$model,
	'discountsInput',
	[
		'class' => 'hidden-discounts-values',
		'value' => $model->getDiscountsInput(),
	]
) ?>

<br/>
<div class="promo-inputs">
	<?php foreach ($products as $product) {
		echo Html::beginTag('div', ['class' => 'form-group m-b-lg',]),
		Html::label(
			'Скидка на продукт "' . $product->label . '"',
			'discount_' . $product->id,
			[
				'class' => 'control-label',
			]
		),
		TouchSpin::widget(
			[
				'name' => 'discount_' . $product->id,
				'pluginOptions' => [
					'min' => -100,
					'max' => 100,
					'step' => 0.1,
					'decimals' => 2,
					'postfix' => '%',
					'buttonup_txt' => '+',
					'buttondown_txt' => '-',
				],
				'options' => [
					'data-product-id' => $product->id,
					'class' => 'form-control di discount',
					'data-value' => $model->getDiscount($product->id, $promoId)
				],
			]
		),
		Html::label(
			'Баллы за продукт "' . $product->label . '"',
			'ball_' . $product->id,
			[
				'class' => 'control-label',
			]
		),
		TouchSpin::widget(
			[
				'name' => 'ball_' . $product->id,
				'pluginOptions' => [
					'min' => -100,
					'max' => 100,
					'step' => 0.1,
					'decimals' => 2,
					'postfix' => '%',
					'buttonup_txt' => '+',
					'buttondown_txt' => '-',
				],
				'options' => [
					'data-product-id' => $product->id,
					'class' => 'form-control di ball',
					'data-value' => $model->getBall($product->id, $promoId)
				],
			]
		),
		Html::endTag('div');
	} ?>
</div>
<div class="form-group">
	<?= Html::submitButton(
		$model->isNewRecord ? 'Создать' : 'Сохранить',
		['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
	) ?>
</div>

<?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model promo\models\Promo */
/* @var array $products */

$this->title = 'Редактировать промо-код: ' . ' ' . $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="promo-update">
	<div class="col-lg-5 hpanel">
		<div class="panel-body">

			<?= $this->render(
				'_form',
				[
					'model' => $model,
					'products' => $products,
					'promo' => $model,
				]
			) ?>
		</div>
	</div>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model promo\models\Promo */
/* @var array $products */

$this->title = 'Новый промо-код';
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-create">
	<div class="col-lg-5 hpanel">
		<div class="panel-body">
			<h1><?= Html::encode($this->title) ?></h1>

			<?= $this->render(
				'_form',
				[
					'model' => $model,
					'products' => $products,
					'promo' => $promo,
				]
			) ?>

		</div>

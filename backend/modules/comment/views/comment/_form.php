<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use user\models\User;

/* @var $this yii\web\View */
/* @var $model comment\models\Comment */
/* @var $form yii\widgets\ActiveForm */

$users = User::find()->where('status != :status', ['status' => User::STATUS_DELETED])->all();
?>

<div class="comment-form">

	<?php $form = ActiveForm::begin(
		[
			'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
				'labelOptions' => ['class' => 'col-sm-2 control-label'],
			],
		]
	); ?>

	<?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map($users, 'id', 'username')) ?>

	<?= $form->field($model, 'type')->textInput() ?>

	<?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'label')->textInput(['maxlength' => 255]) ?>
	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-2">
			<?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	<?php ActiveForm::end(); ?>

</div>

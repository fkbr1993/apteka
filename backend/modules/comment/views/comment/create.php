<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model comment\models\Comment */

$this->title = 'Новое сообщение';
$this->params['breadcrumbs'][] = ['label' => 'Комментарии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-create">

	<div class="col-lg-5 hpanel">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

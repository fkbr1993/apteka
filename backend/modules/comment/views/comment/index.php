<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel comment\models\SearchComment */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-8 comment-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
		<?= Html::a('Новое сообщение', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<div class="hpanel">
		<div class="panel-body">
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>

			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'summary' => '',
					'options' => ['class' => 'table-responsive'],
					'tableOptions' => ['class' => 'table no-footer'],
					'columns' => [
						'id',
						[
							'attribute' => 'username',
							'format' => 'raw',
							'value' => function ($model) {
								return '<a href="' . Url::to(
									['/user/user/view', 'id' => $model->user_id]
								) . '">' . $model->username . '</a>';
							},
						],
						'content:ntext',
						'label',
						[
							'format' => 'raw',
							'value' => function ($model) {
								if ($model->type) {
									$button = '';
								} else {
									$button = '<a href="#admin-comment-modal" class="btn btn-primary admin-send-message" data-user="' . $model->user->username . '" data-id="' . $model->user_id . '" data-toggle="modal">Ответить</a>';
								}

								return $button;
							}
						],
						['class' => 'backend\components\CircleActionColumn'],
					],
				]
			);
			?>

		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>
<?php
$form = ActiveForm::begin(
	[
		'id' => 'comment-form',
		'options' => ['class' => 'form-horizontal'],
		'action' => Url::to(['/comment/comment/send']),
	]
);
?>
<!-- Modal -->
<div class="modal fade" id="admin-comment-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="comment-modal-padding">
					<?= $form->field($model, 'label')->label('Заголовок'); ?>
					<?= $form->field($model, 'content')->textarea(['rows' => 5])->label(false); ?>
					<?= $form->field($model, 'user_id')->hiddenInput(['id' => 'admin-user-id'])->label(false) ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php ActiveForm::end(); ?>

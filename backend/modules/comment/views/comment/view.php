<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model comment\models\Comment */

$this->title = $model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">
	<div class="col-lg-5 hpanel">
		<p class="m-b-md">
			<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			<?= Html::a(
				'Удалить',
				['delete', 'id' => $model->id],
				[
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => 'Вы уверены, что хотите удалить?',
						'method' => 'post',
					],
				]
			) ?>
		</p>
		<div class="panel-body">

			<?= DetailView::widget(
				[
					'model' => $model,
					'options' => ['class' => 'table no-footer'],
					'attributes' => [
						'id',
						'user.username',
						'type',
						'content:ntext',
						'label',
					],
				]
			) ?>
		</div>
	</div>

</div>

<?php

namespace comment;

class CommentModule extends \yii\base\Module
{
    public $controllerNamespace = 'comment\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace comment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use comment\models\Comment;

/**
 * SearchComment represents the model behind the search form about `comment\models\Comment`.
 */
class SearchComment extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['content', 'label'], 'safe'],
	        [['username'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find()->select(['user.username username','comment.*'])->joinWith('user')->orderBy('comment.id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort' => false,
        ]);

//	    $dataProvider->sort->attributes['username'] = [
//		    'asc' => ['user.username' => SORT_ASC],
//		    'desc' => ['user.username' => SORT_DESC],
//	    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'username' => $this->username,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'label', $this->label]);

        return $dataProvider;
    }
}

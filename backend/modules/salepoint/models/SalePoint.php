<?php

namespace salepoint\models;

use yii\helpers\ArrayHelper;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use yii\helpers\Html;

use Yii;

/**
 * This is the model class for table "salepoint".
 *
 * @property integer $id
 * @property string $address
 * @property integer $image_id
 *
 */
class SalePoint extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'salepoint';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => true,
					'required' => false,
				]
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['address'], 'required']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'address' => 'Адрес',
		];
	}

	/**
	 * Формируем поля для формы
	 *
	 * @return array
	 */
	public function prepareForm()
	{
		return [
			'id',
			'address',
			[
				'attribute' => 'Изображение',
				'format' => 'raw',
				'value' => $this->image_id
					? Html::img(
						\metalguardian\fileProcessor\helpers\FPM::src($this->image_id, 'salepoint', 'preview')
					)
					: null,
			],
		];
	}
}

<?php

namespace salepoint\controllers;

use Faker\Factory;
use Yii;
use salepoint\models\SalePoint;
use salepoint\models\SearchSalePoint;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use yii\filters\AccessControl;

/**
 * SalePointController implements the CRUD actions for SalePoint model.
 */
class SalepointController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['view', 'create', 'update', 'delete', 'index', 'fake'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}


	public function actionFake()
	{
		if (User::isAdmin()) {
			$faker = Factory::create();
			for ($i = 0; $i < 10; $i++) {
				$model = new SalePoint;
				$model->address = $faker->address;
				$model->save();
			}
		}

		$this->redirect(['/salepoint/salepoint']);
	}

	/**
	 * Lists all SalePoint models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SearchSalePoint();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Displays a single SalePoint model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
			]
		);
	}

	/**
	 * Creates a new SalePoint model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new SalePoint();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render(
				'create',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * Updates an existing SalePoint model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render(
				'update',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * Deletes an existing SalePoint model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the SalePoint model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return SalePoint the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = SalePoint::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

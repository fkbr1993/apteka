<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model salepoint\models\SalePoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sale-point-form">

	<?php $form = \kartik\form\ActiveForm::begin(
		[
			'enableClientValidation' => false,
			'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
			'fieldConfig' => [
				'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
				'labelOptions' => ['class' => 'col-sm-2 control-label'],
			],
		]
	); ?>

	<?= $form->field($model, 'address')->textInput(['maxlength' => 255]) ?>
	<?= $form->field($model, 'image_id')->widget(\kartik\file\FileInput::classname(), [
		'pluginOptions' => [
			'showCaption' => false,
			'showRemove' => false,
			'showUpload' => false,
			'browseClass' => 'btn btn-default btn-md-1',
			'browseIcon' => '',
			'browseLabel' =>  'Загрузить фото'
		],
		'options' => ['accept' => 'image/*'],
	])->label('Изображение') ?>


	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-2">
			<?= Html::resetButton('Сросить', ['class' => 'btn btn-default']) ?>
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>

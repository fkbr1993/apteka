<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model salepoint\models\SalePoint */

$this->title = 'Редактировние торговой точки: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Торговые точки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="sale-point-update">

	<div class="col-lg-5 hpanel">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

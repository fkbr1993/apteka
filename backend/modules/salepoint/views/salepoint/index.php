<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel salepoint\models\SearchSalePoint */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Торговые точки';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-8 sale-point-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
		<?= Html::a('Новая торговая точка', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<div class="hpanel">
		<div class="panel-body">
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>

			<p>Поиск торговой точки
				<small class="pull-right">БетулаФарм
					<sup>®</sup>
				</small>
			</p>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => '/salepoint/salepoint/index',
				]
			) ?>
			<div class="input-group">
				<?= $form->field(
					$searchModel,
					'address',
					[
						'template' => '{input}',

					]
				)->input(
					'text',
					[
						'class' => 'input form-control',
						'placeholder' => 'Адрес или ID'
					]
				) ?>
				<span class="input-group-btn">
						<button class="btn btn btn-success" type="submit">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
			</div>
			<?php $form->end() ?>

			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'summary' => '',
					'options' => ['class' => 'table-responsive m-t-lg'],
					'tableOptions' => ['class' => 'table table-striped table-hover no-footer'],
					'columns' => [
						'address',
						['class' => 'backend\components\CircleActionColumn'],
					],
				]
			); ?>
		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>

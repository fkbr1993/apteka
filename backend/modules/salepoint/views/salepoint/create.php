<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model salepoint\models\SalePoint */

$this->title = 'Новая торговая точка';
$this->params['breadcrumbs'][] = ['label' => 'Торговые точки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sale-point-create">

	<div class="col-lg-5 hpanel">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model salepoint\models\SalePoint */

$this->title = $model->address;
$this->params['breadcrumbs'][] = ['label' => 'Торговые точки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-5 hpanel">
	<p class="m-b-md">
		<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(
			'Удалить',
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Вы уверены, что хотите удалить?',
					'method' => 'post',
				],
			]
		) ?>
	</p>
	<div class="panel-body">

		<?= DetailView::widget(
			[
				'model' => $model,
				'options' => ['class' => 'table no-footer'],
				'attributes' => $model->prepareForm(true),
			]
		) ?>
	</div>
</div>

<?php

namespace salepoint;

class SalePointModule extends \yii\base\Module
{
    public $controllerNamespace = 'salepoint\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

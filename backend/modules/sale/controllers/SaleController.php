<?php

namespace sale\controllers;

use Yii;
use sale\models\Sale;
use sale\models\SearchSale;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use product\models\Product;
use discount\models\Discount;
use promo\models\Promo;

/**
 * SaleController implements the CRUD actions for Sale model.
 */
class SaleController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['view', 'create', 'update', 'delete', 'index', 'status'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post', 'status'],
				],
			],
		];
	}

	/**
	 * Lists all Sale models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SearchSale();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render(
			'index',
			[
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]
		);
	}

	/**
	 * Displays a single Sale model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render(
			'view',
			[
				'model' => $this->findModel($id),
			]
		);
	}

	/**
	 * Creates a new Sale model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Sale();

		if ($model->load(Yii::$app->request->post())) {
			$model->amount = $model->amount ? $model->amount : 1;
			$product = Product::findOne($model->product_id);
			if ($product) {
				$model->calculateBalls($product);
				$model->created_at = time();
				if ($model->save()) {
					$user = User::findOne($model->user_id);
					if ($user && $model->status == Sale::STATUS_PAID) {
						$user->ball += $model->ball;
						$user->save(false);
					}

					return $this->redirect(['view', 'id' => $model->id]);
				}
			}
		} else {
			return $this->render(
				'create',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * Updates an existing Sale model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$oldUserId = $model->user_id;

		if ($model->load(Yii::$app->request->post())) {
			$model->amount = $model->amount ? $model->amount : 1;
			$product = Product::findOne($model->product_id);
			if ($product) {
				$model->calculateBalls($product);
				if ($model->save()) {
					if ($model->status == Sale::STATUS_PAID) {
						$oldUser = User::findOne($oldUserId);
						if ($model->user_id != $oldUserId) {
							$user = User::findOne($model->user_id);
							if ($user) {
								$user->ball += $model->ball;
								$user->save(false);
							}
							if ($oldUser) {
								$oldUser->ball -= $model->oldBall;
								$oldUser->save(false);
							}
						} else {
							if ($oldUser) {
								$oldUser->ball -= $model->oldBall;
								$oldUser->ball += $model->ball;
								$oldUser->save(false);
							}
						}
					}
					return $this->redirect(['view', 'id' => $model->id]);
				}
			}
		} else {
			return $this->render(
				'update',
				[
					'model' => $model,
				]
			);
		}
	}

	/**
	 * id продажи
	 * @param $id
	 *
	 * Меняем статус продажи на оплачено
	 *
	 * @return \yii\web\Response
	 */
	public function actionStatus($id, $type)
	{
		$model = Sale::findOne($id);

		if ($model) {
			if ($type == Sale::STATUS_PAID) {
				$model->status = Sale::STATUS_PAID;
			} else {
				$model->status = Sale::STATUS_NOT_PAID;
			}
			$user = User::findOne($model->user_id);
			$model->save(false);
			if ($user) {
				if ($type == Sale::STATUS_PAID) {
					$user->ball += $model->ball;
				} else {
					$user->ball -= $model->ball;
				}
				$user->save(false);
			}
		}

		return $this->redirect(['view', 'id' => $model->id]);
	}

	/**
	 * Deletes an existing Sale model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$sale = $this->findModel($id);
		$user = User::findOne($sale->user_id);
		$user->ball = $user->ball - $sale->ball;
		if ($sale->delete()) {
			$user->save();
		}

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Sale model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Sale the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Sale::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

<?php

namespace sale\models;

use discount\models\Discount;
use product\models\Product;
use promo\models\Promo;
use salepoint\models\SalePoint;
use user\models\User;
use Yii;

/**
 * This is the model class for table "sale".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property string $amount
 * @property string $ball
 * @property integer $created_at
 * @property string $salepoint_id
 * @property string $client_name
 * @property string $client_last_name
 * @property integer $status
 * @property string $price
 * @property integer $order_id
 */
class Sale extends \yii\db\ActiveRecord
{

	/**
	 * Логин
	 *
	 * @var
	 */
	public $username;

	/**
	 * Заголовок
	 *
	 * @var
	 */
	public $label;

	/**
	 * Адрес
	 *
	 * @var
	 */
	public $address;

	/**
	 * @var
	 */
	public $oldBall;

	/**
	 * Не оплачена
	 */
	const STATUS_NOT_PAID = 0;

	/**
	 * Оплачена
	 */
	const STATUS_PAID = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sale';
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'product_id', 'salepoint_id'], 'required'],
			[['user_id', 'product_id', 'salepoint_id', 'created_at', 'amount', 'status', 'order_id'], 'integer'],
			[['product_id', 'amount', 'client_name', 'client_last_name', 'status',], 'safe'],
			['ball', 'number']
		];
	}


	/**
	 * После удаления продажи убираем баллы
	 *
	 * @return bool
	 */
	public function afterDelete()
	{
		$user = User::findOne(Yii::$app->user->id);
		$user->ball = $user->ball - $this->ball;
		$user->save();

		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'username' => 'Пользователь',
			'label' => 'Продукт',
			'amount' => 'Количество',
			'address' => 'Адрес',
			'ball' => 'Баллы',
			'created_at' => 'Дата создания',
			'client_name' => 'Имя покупателя',
			'client_last_name' => 'Фамилия покупателя',
			'salepoint_id' => 'Адрес',
			'user_id' => 'Пользователь',
			'product_id' => 'Продукт',
			'status' => 'Статус',
			'order_id' => '№ заказа',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSalepoint()
	{
		return $this->hasOne(SalePoint::className(), ['id' => 'salepoint_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @param $product
	 *
	 * Подсчитываем баллы
	 */
	public function calculateBalls($product) {
		$ball = 0;
		$promo = Promo::findOne(['archive' => 0, 'user_id' => $this->user_id]);
		$discounts = $promo ? $promo->discounts : [];
		if ($discount = Discount::findProductDiscount($discounts, $product->id)) {
			$ball = $discount->ball ? $discount->ball : 0;
			$totalPrice = ($product->price - $discount->discount * ($product->price) / 100) * $this->amount;
		} else {
			$totalPrice = $product->price * $this->amount;
		}
		$totalBall = ($totalPrice / 100) * $ball;

		$this->oldBall = $this->ball;

		$this->price = $totalPrice;
		$this->ball = $totalBall;
	}
}

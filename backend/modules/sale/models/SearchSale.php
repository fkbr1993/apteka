<?php

namespace sale\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use sale\models\Sale;

/**
 * SearchSale represents the model behind the search form about `sale\models\Sale`.
 */
class SearchSale extends Sale
{

	public $timestamp_from;

	public $timestamp_to;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'ball', 'order_id'], 'integer'],
			[['amount', 'timestamp_from', 'timestamp_to'], 'safe'],
			[['address', 'username', 'label'], 'string']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Sale::find()->select(
			['sale.*', 'user.username username', 'product.label label', 'salepoint.address address']
		)->joinWith(['user', 'product', 'salepoint']);

		$dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
				'sort' => [
					'attributes' => ['order_id', 'status', 'id'],
					'defaultOrder' => [
						'id' => SORT_DESC,
					],
				],
			]
		);

//
//	    $dataProvider->sort->attributes['label'] = [
//		    'asc' => ['product.label' => SORT_ASC],
//		    'desc' => ['product.label' => SORT_DESC],
//	    ];
//
//	    $dataProvider->sort->attributes['address'] = [
//		    'asc' => ['salepoint.address' => SORT_ASC],
//		    'desc' => ['salepoint.address' => SORT_DESC],
//	    ];

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere(
			[
				'order_id' => $this->order_id,
				'username' => $this->username,
				'label' => $this->label,
				'ball' => $this->ball,
				'status' => $this->status,
			]
		);
		if ($this->timestamp_from) {
			$from = strtotime(str_replace('/', '.',$this->timestamp_from));
			$query->andFilterWhere(['>=', 'sale.created_at', $from]);
		}
		if ($this->timestamp_to) {
			$to = strtotime(str_replace('/', '.',$this->timestamp_to)) + 86399;
			$query->andFilterWhere(['<=', 'sale.created_at', $to]);
		}

		$query->andFilterWhere(['like', 'amount', $this->amount])
			->andFilterWhere(['like', 'address', $this->address]);

		return $dataProvider;
	}
}

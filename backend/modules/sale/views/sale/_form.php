<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use user\models\User;
use yii\helpers\ArrayHelper;
use product\models\Product;
use salepoint\models\SalePoint;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model sale\models\Sale */
/* @var $form yii\widgets\ActiveForm */

$users = User::find()->all();
$products = Product::find()->all();
$salepoints = SalePoint::find()->all();
?>

<div class="sale-form">

	<?php $form = ActiveForm::begin(
		[
			'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
				'labelOptions' => ['class' => 'col-sm-2 control-label'],
			],
		]
	); ?>

	<?= $form->field($model, 'order_id')->textInput() ?>

	<?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map($users, 'id', 'username')) ?>

	<?= $form->field($model, 'product_id')->dropDownList(ArrayHelper::map($products, 'id', 'label')) ?>

	<?= $model->isNewRecord  ? $form->field($model, 'status')->dropDownList([0=>'Не оплачена', 1=>'Оплачена']) : '' ?>

	<?= $form->field($model, 'amount')->widget(
		TouchSpin::classname(),
		[
			'pluginOptions' => [
				'buttonup_class' => 'btn btn-default bootstrap-touchspin-up',
				'buttondown_class' => 'btn btn-default bootstrap-touchspin-down',
				'buttonup_txt' => '+',
				'buttondown_txt' => '-'
			]
		]
	); ?>

	<?= $form->field($model, 'client_name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'client_last_name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'salepoint_id')->dropDownList(ArrayHelper::map($salepoints, 'id', 'address')) ?>

	<?php // $form->field($model, 'ball')->textInput(['maxlength' => 255]) ?>

	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-2">
			<?= Html::resetButton('Сросить', ['class' => 'btn btn-default']) ?>
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>

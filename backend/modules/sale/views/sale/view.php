<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use sale\models\Sale;

/* @var $this yii\web\View */
/* @var $model sale\models\Sale */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Продажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-5 hpanel">
	<p class="m-b-md">
		<?= !$model->status ? Html::a(
			'Оплачено',
			['status', 'id' => $model->id, 'type' => Sale::STATUS_PAID],
			[
				'class' => 'btn btn-success',
				'data' => [
					'confirm' => 'Вы уверены, что хотите сменить статус на "Оплачено"?',
					'method' => 'post',
				],
			]
		) : Html::a(
			'Не оплачено',
			['status', 'id' => $model->id, 'type' => Sale::STATUS_NOT_PAID],
			[
				'class' => 'btn btn-warning',
				'data' => [
					'confirm' => 'Вы уверены, что хотите сменить статус на "Не оплачено"?',
					'method' => 'post',
				],
			]
		) ?>
		<?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(
			'Удалить',
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Вы уверены, что хотите удалить?',
					'method' => 'post',
				],
			]
		) ?>
	</p>
	<div class="panel-body">

		<?= DetailView::widget(
			[
				'model' => $model,
				'options' => ['class' => 'table no-footer'],
				'attributes' => [
					'id',
					'order_id',
					'user.username',
					'product.label',
					'amount',
					'client_name',
					'client_last_name',
					'salepoint.address',
					'ball',
					[
						'label' => 'Статус',
						'value' => ($model->status) ? 'Оплачена' : 'Не оплачена',
					],
				],
			]
		) ?>
	</div>
</div>

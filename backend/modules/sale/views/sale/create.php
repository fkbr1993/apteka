<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model sale\models\Sale */

$this->title = 'Новая продажа';
$this->params['breadcrumbs'][] = ['label' => 'Продажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sale-create">

	<div class="col-lg-5 hpanel">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
			<?= $this->render(
				'_form',
				[
					'model' => $model,
				]
			) ?>
		</div>
	</div>

</div>

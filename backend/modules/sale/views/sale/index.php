<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use metalguardian\fileProcessor\helpers\FPM;
use kartik\date\DatePicker;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel sale\models\SearchSale */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продажи';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
	'order_id',
	'label' => [
		'attribute' => 'label',
		'format' => 'raw',
		'value' => function ($model) {
			return $model->label;
		}
	],
	'created_at' => [
		'attribute' => 'created_at',
		'format' => 'raw',
		'value' => function ($model) {
			return Yii::$app->formatter->asDatetime(
				$model->created_at,
				'php:d/m/Y'
			);
		}
	],
	[
		'label' => 'ID пользов.',
		'value' => function ($model) {
			return $model->user_id;
		}
	],
	'username',
	'ball',
	[
		'label' => 'Цена',
		'format' => ['decimal', 2],
		'hAlign' => 'right',
		'width' => '110px',
		'value' => function ($model) {
			return $model->price;
		}
	],
];



?>
<div class="col-lg-8 sale-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p class="m-b-md">
		<?= Html::a('Новая продажа', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<div class="hpanel">
		<div class="panel-body">
			<h2 class="font-light">
				<?= Html::encode($this->title) ?>
			</h2>

			<p>Поиск по дате продажи
				<small class="pull-right">БетулаФарм
					<sup>®</sup>
				</small>
			</p>
			<?php $form = \yii\widgets\ActiveForm::begin(
				[
					'method' => 'get',
					'action' => ['/sale/sale/index'],
				]
			) ?>
			<div class="form-group">
				<div class="input-group">
					<?= DatePicker::widget(
						[
							'model' => $searchModel,
							'attribute' => 'timestamp_from',
							'attribute2' => 'timestamp_to',
							'separator' => 'До',
							'type' => DatePicker::TYPE_RANGE,
							'form' => $form,
							'pluginOptions' => [
								'format' => 'dd/mm/yyyy',
								'autoclose' => true,
							]
						]
					) ?>
					<span class="input-group-btn">
						<button class="btn btn btn-success" type="submit">
							<i class="fa fa-search"></i>
							Поиск
						</button>
					</span>
				</div>
			</div>
			<?php $form->end() ?>
			<div class="pull-right">
				<?= ExportMenu::widget(
					[
						'dataProvider' => $dataProvider,
						'columns' => $gridColumns,
						'fontAwesome' => true,
						'exportConfig' => [
							ExportMenu::FORMAT_PDF => false
						],
					]
				) ?>
			</div>
			<span class="clearfix"></span>
			<?= GridView::widget(
				[
					'dataProvider' => $dataProvider,
					'options' => ['class' => 'table-responsive m-t-lg'],
					'tableOptions' => ['class' => 'table no-footer'],
					'summary' => '',
					'columns' => [
//						[
//							'attribute' => 'image_id',
//							'format' => 'raw',
//							'label' => false,
//							'contentOptions' => ['class' => 'col-md-1 text-center'],
//							'value' => function ($model) {
//								/** @var $model sale\models\Sale */
//								return $model->product->image_id ? Html::img(
//									FPM::src($model->product->image_id, 'banner', 'small')
//								) : '';
//							}
//						],
						'label' => [
							'attribute' => 'label',
							'format' => 'raw',
							'value' => function ($model) {
								return $model->label . '<br/><small>' . Yii::$app->formatter->asDatetime(
									$model->created_at,
									'php:d/m/Y'
								) . '</small>';
							}
						],
						'order_id',
						[
							'label' => 'ID пользов.',
							'format' => 'raw',
							'value' => function ($model) {
								return Html::a($model->user_id, ['/user/user/view', 'id' => $model->user_id]);
							}
						],
//						[
//							'attribute' => 'username',
//							'format' => 'raw',
//							'value' => function ($model) {
//								return '<a href="' . Url::to(
//									['/user/user/view', 'id' => $model->user_id]
//								) . '">' . $model->username . '</a>';
//							},
//						],
						[
							'attribute' => 'client_last_name',
							'label' => 'Покупатель',
							'format' => 'raw',
							'value' => function ($model) {
								return Html::a($model->client_name . ' ' . $model->client_last_name, ['/user/user/view', 'id' => $model->user_id]);
							},
						],
						'amount',
						'ball',
						[
							'label' => 'Цена',
							'format' => 'raw',
							'value' => function ($model) {
								return $model->price . ' <i class="fa fa-rub"></i>';
							}
						],
						[
							'attribute' => 'status',
							'value' => function($model){return $model->status ? 'Оплачена' : 'Не оплачена';},
							'filter' => [0 => 'Не оплачена', 1=>'Оплачена'],
						],
						['class' => 'backend\components\CircleActionColumn'],
					],
				]
			); ?>
		</div>
		<div class="panel-footer">
			Полей - <?= $dataProvider->count ?>
		</div>
	</div>
</div>


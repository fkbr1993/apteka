<?php

namespace sale;

class SaleModule extends \yii\base\Module
{
    public $controllerNamespace = 'sale\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

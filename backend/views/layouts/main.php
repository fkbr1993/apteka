<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\models\User;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */


\backend\assets\BackAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="header">
	<div class="color-line">
	</div>
	<div class="light-version" id="logo">
        <span>
            Администратор
        </span>
	</div>
	<nav role="navigation">
		<div class="header-link hide-menu">
			<i class="fa fa-bars"></i>
		</div>
		<div class="navbar-right">
			<ul class="nav navbar-nav no-borders">
				<li class="dropdown">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle label-menu-corner" aria-expanded="false">
						<i class="pe-7s-mail"></i>
						<?php $comments = \comment\models\Comment::getUnreadMessages() ?>
						<span class="label label-success"><?= count($comments) ?></span>
					</a>
					<ul class="dropdown-menu hdropdown animated flipInX">
						<div class="title">
							У вас <?= count($comments) ?> новых сообщений
						</div>
						<?php foreach ($comments as $comment) : ?>
							<li>
								<a href="<?= Url::to(['/comment/comment/view', 'id' => $comment->id]) ?>">
									<?= $comment->label ?>
								</a>
							</li>
						<?php endforeach ?>
						<li class="summary">
							<a href="<?= Url::to(['/comment/comment/index']) ?>">Все сообщения</a>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="<?= Url::to(['/site/logout']) ?>" data-method="post">
						<i class="pe-7s-upload pe-rotate-90"></i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</div>
<aside id="menu">
	<div id="navigation">
		<div class="profile-picture">
			<a href="<?= Url::toRoute(['/user/user/index']) ?>">
				<img alt="logo" class="img-circle m-b" src="<?= User::avatar() ?>">
			</a>

			<div class="stats-label text-color">
				<span class="font-extra-bold font-uppercase"><?= Yii::$app->user->identity->username ?></span>
			</div>
		</div>

		<ul id="side-menu" class="nav">
			<li>
				<a href="#">
					<span class="nav-label">Пользователи</span>
					<?php
					$new_users = \user\models\User::countNewUsers();
					if ($new_users) : ?>
						<span class="label label-success pull-right">+<?= $new_users ?></span>
					<?php else : ?>
						<span class="fa arrow"></span>
					<?php endif ?>
				</a>
				<ul class="nav nav-second-level collapse">
					<?php if ($new_users) : ?>
						<li>
							<a class="<?= $new_users ? 'text-success' : '' ?>" href="<?= Url::to(
								['/user/user/new_users']
							) ?>">Новые
							</a>
						</li>
					<?php endif ?>
					<li>
						<a href="<?= Url::to(['/user/user/index']) ?>">Все пользователи</a>
					</li>
					<li>
						<a href="<?= Url::to(['/user/user/index', 'SearchUser[status]' => 0]) ?>">Архив</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="<?= Url::to(['/product/product/index']) ?>">
					<span class="nav-label">Продукция</span>
				</a>
			</li>
			<li>
				<a href="<?= Url::to(['/sale/sale/index']) ?>">
					<span class="nav-label">Продажи</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="nav-label">Промо-коды</span>
					<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li><a href="<?= Url::to(['/promo/promo/index']) ?>">Все промокоды</a></li>
					<li>
						<a href="<?= Url::to(['/promo/promo/index', 'archive' => 1]) ?>">Архив</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="<?= Url::to(['/comment/comment/index']) ?>">
					<span class="nav-label">Сообщения</span>
				</a>
			</li>
			<li>
				<a href="<?= Url::to(['/salepoint/salepoint/index']) ?>">
					<span class="nav-label">Торговые точки</span>
				</a>
			</li>
		</ul>
	</div>
</aside>

<div id="wrapper">
	<?php if (isset($this->params['breadcrumbs']) && (Yii::$app->controller->action->id == 'update' || Yii::$app->controller->action->uniqueId == 'product/product/index')) : ?>
		<div class="normalheader transition animated fadeIn">
			<div class="hpanel">
				<div class="panel-body">
					<h2 class="font-light m-b-xs">
						<?= end($this->params['breadcrumbs']) ?>
					</h2>
					<?php if (Yii::$app->controller->action->id == 'update') : ?>
						<small class="ng-binding"><?= $this->params['breadcrumbs'][1]['label'] ?></small>
					<?php endif ?>
				</div>
			</div>
		</div>
	<?php endif ?>
	<div class="content animate-panel clearfix">
		<?= $content ?>
	</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

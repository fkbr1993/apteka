<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-container">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center m-b-md">
				<h3><?= Html::encode($this->title) ?></h3>
			</div>
			<div class="hpanel">
				<div class="panel-body">
					<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
					<?= $form->field($model, 'email') ?>
					<?= $form->field($model, 'password')->passwordInput() ?>
					<?= $form->field($model, 'rememberMe')->checkbox() ?>
					<div class="form-group">
						<?= Html::submitButton('Вход', ['class' => 'btn btn-success btn-block', 'name' => 'login-button']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
